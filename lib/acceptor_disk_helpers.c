#include <sys/time.h>
#include "db.h"
#include "limits.h"


#include "event_counters.h"
/* BDB Access method, only 1 should be defined */
// #define BDB_USE_BTREE 
// #define BDB_USE_QUEUE
#define BDB_USE_RECNO 
// #define BDB_USE_HASH 

DB *acc_db; /* Database containing inventory information */
// FILE * errorlog;


#ifdef COUNT_PAXOS_EVENTS
#define N_OF_SYNC_SAMPLES 1000
unsigned int prepare_sync_samples[N_OF_SYNC_SAMPLES];
unsigned int accept_sync_samples[N_OF_SYNC_SAMPLES];
int prepare_sync_samples_count = 0;
int accept_sync_samples_count = 0;
unsigned int max_prepare_timediff = 0;
unsigned int min_prepare_timediff = UINT_MAX;
unsigned int max_accept_timediff = 0;
unsigned int min_accept_timediff = UINT_MAX;
struct timeval before_sync;
struct timeval after_sync;

void set_time_before_sync(int cause) {
    if((cause == PAXOS_PREPARE && prepare_sync_samples_count >= N_OF_SYNC_SAMPLES) || (cause == PAXOS_ACCEPT && accept_sync_samples_count >= N_OF_SYNC_SAMPLES)) {
        return;
    }
    gettimeofday(&before_sync, NULL);
}
void set_time_after_sync(int cause) {
    if((cause == PAXOS_PREPARE && prepare_sync_samples_count >= N_OF_SYNC_SAMPLES) || (cause == PAXOS_ACCEPT && accept_sync_samples_count >= N_OF_SYNC_SAMPLES)) {
        return;
    }
    
    gettimeofday(&after_sync, NULL);
    unsigned int timediff;
    timediff = (after_sync.tv_usec - before_sync.tv_usec);
    timediff += (after_sync.tv_sec - before_sync.tv_sec) * 1000000;
    if(cause == PAXOS_PREPARE) {
        prepare_sync_samples[prepare_sync_samples_count] = timediff;
        prepare_sync_samples_count++;
        if(timediff > max_prepare_timediff) {
            max_prepare_timediff = timediff;
        }
        if(timediff < min_prepare_timediff) {
            min_prepare_timediff = timediff;
        }
    } else if (cause == PAXOS_ACCEPT) {
        accept_sync_samples[accept_sync_samples_count] = timediff;
        accept_sync_samples_count++;
        if(timediff > max_accept_timediff) {
            max_accept_timediff = timediff;
        }
        if(timediff < min_accept_timediff) {
            min_accept_timediff = timediff;
        }
    } else {
        printf("Invalid cause in set_time_after_sync:%d\n", cause);
    }

}

void acceptor_print_event_counters() {
    #ifndef PAXOS_ACCEPTOR_FORCE_DISK_FLUSH
    return;
    #endif
    
    if(!PAXOS_ACCEPTOR_FORCE_DISK_FLUSH) {
        return;
    }
    
    printf("DB Synchronization time averages: (usec)\n");
    long unsigned int sum = 0;
    double average;
    size_t i;

    for(i = 0; i < prepare_sync_samples_count; i++) {
        sum += prepare_sync_samples[i];    
    }
    average = sum/prepare_sync_samples_count;
    printf("Sync after prepare:\n");
    printf("Samples:%d  Min:%d  Max:%d  Avg:%d (Tot: %d sec)\n", prepare_sync_samples_count, (int)min_prepare_timediff, (int)max_prepare_timediff, (int)average, (int)(sum/1000000));
    
    sum = 0;
    for(i = 0; i < accept_sync_samples_count; i++) {
        sum += accept_sync_samples[i];    
    }
    average = sum/accept_sync_samples_count;
    printf("Sync after accept:\n");
    printf("Samples:%d  Min:%d  Max:%d  Avg:%d (Tot: %d sec)\n", accept_sync_samples_count, (int)min_accept_timediff, (int)max_accept_timediff, (int)average, (int)(sum/1000000));
}

#else
void acceptor_print_event_counters() {
    printf("Event counters are disabled!\n");
}
void set_time_before_sync(int cause) {
}
void set_time_after_sync(int cause) {  
}
#endif

int disk_init_permastorage(char* acc_db_name) {
    acc_db = NULL;
    
    u_int32_t open_flags;
    int ret;

    remove(acc_db_name);

    ret = db_create(&acc_db, NULL, 0);
    if (ret != 0) {
        fprintf(stderr, "Create: %s: %s\n", acc_db_name, db_strerror(ret));
        return(ret);
    }


    DBTYPE access_method;
#ifdef BDB_USE_BTREE
    access_method = DB_BTREE;
#endif
#ifdef BDB_USE_RECNO
    access_method = DB_RECNO;
#endif
#ifdef BDB_USE_HASH
    access_method = DB_HASH;
#endif
#ifdef BDB_USE_QUEUE
    access_method = DB_QUEUE;
    int max_db_record_size = sizeof(acceptor_record) + PAXOS_MAX_VALUE_SIZE;
    if (PAXOS_MAX_VALUE_SIZE > 8000) {
        printf("Paxos max value size is set too high for using QUEUE\n");
        return(-1);
    }
    ret = acc_db->set_pagesize(acc_db, (4096*2)); 
    if (ret != 0) {
        acc_db->err(acc_db, ret, "Database set pagesize failed.");
        return(ret);
    } 
    ret = acc_db->set_re_len(acc_db, max_db_record_size);
    if (ret != 0) {
        acc_db->err(acc_db, ret, "Database set record size to %d failed.", max_db_record_size);
        return(ret);
    }
    
#endif
    open_flags = DB_CREATE;
    
    ret = acc_db->open(acc_db, 
                    NULL,      
                    acc_db_name,  
                    NULL,     
                    access_method,  
                    open_flags, 
                    0);         
    if (ret != 0) {
        acc_db->err(acc_db, ret, "Database '%s' open failed.", acc_db_name);
        return(ret);
    }
printf("ok\n");
                                                                                                                               
    return 0;
}

int disk_force_syncrony(int cause) {
    if(PAXOS_ACCEPTOR_FORCE_DISK_FLUSH) {
        LOG(V_DBG, ("Forcing disk synchrony\n"));
        set_time_before_sync(cause);
        acc_db->sync(acc_db, 0);
        set_time_after_sync(cause);
    } 
    return 0;
}

int disk_cleanup_permastorage() {
    int ret;

    ret =  acc_db->close(acc_db, 0);
    if (ret != 0) {
        fprintf(stderr, "Database close failed: %s\n", db_strerror(ret));
    }
    return ret;
}
int disk_update_record(acceptor_record* rec) {
    DBT key, data;
    memset(&key, 0, sizeof(DBT));
    memset(&data, 0, sizeof(DBT));
    
    int size = sizeof(acceptor_record) - sizeof(int*) + rec->value_size;

    acceptor_record * flat_rec = malloc(size);

    flat_rec->iid           = rec->iid;
    flat_rec->ballot        = rec->ballot;
    flat_rec->value_ballot  = rec->value_ballot;
    flat_rec->value_size    = rec->value_size;

    memcpy(&flat_rec->value, rec->value, rec->value_size);

    db_recno_t recno = (rec->iid + 1);
    key.data = &recno;
    key.size = sizeof(db_recno_t);


    data.data = flat_rec;
    data.size = size;

    int ret = acc_db->put(acc_db, NULL, &key, &data, 0);
    if (ret != 0) {
        fprintf(stderr, "PUT failed: %s\n", db_strerror(ret));
        return -1;
    }
    free(flat_rec);

    if(PAXOS_ACCEPTOR_FORCE_DISK_FLUSH)
        acc_db->sync(acc_db, 0);
    return ret;
}


acceptor_record* disk_lookup_record(int instance_id) {
    int ret;
    acceptor_record* rec;
    DBT key, data;
    
    memset(&key, 0, sizeof(DBT));
    memset(&data, 0, sizeof(DBT));


    db_recno_t recno = (instance_id + 1);
    key.data = &recno;
    key.size = sizeof(db_recno_t);

    ret = acc_db->get(acc_db, NULL, &key, &data, 0);
    if (ret == DB_NOTFOUND || ret == DB_KEYEMPTY) {
        return NULL;
    }
    
    if (ret != 0) {
        fprintf(stderr, "Get failed: %s\n", db_strerror(ret));
        return NULL;
    }
    acceptor_record * flat_rec = (acceptor_record *) data.data;


    rec = malloc(sizeof(acceptor_record));
    rec->iid           = flat_rec->iid;
    rec->ballot        = flat_rec->ballot;
    rec->value_ballot  = flat_rec->value_ballot;
    rec->value_size    = flat_rec->value_size;
    

    rec->value = malloc(rec->value_size);
    memcpy(rec->value, &flat_rec->value, rec->value_size);
    
    return rec;
}


int disk_lookup_ballot(int instance_id) {
    int ret;
    DBT key, data;
    
    memset(&key, 0, sizeof(DBT));
    memset(&data, 0, sizeof(DBT));

    db_recno_t recno = (instance_id + 1);
    key.data = &recno;
    key.size = sizeof(db_recno_t);

    ret = acc_db->get(acc_db, NULL, &key, &data, 0);    
    if (ret == DB_NOTFOUND || ret == DB_KEYEMPTY) {
        return 0;
    }

    if (ret != 0) {
        fprintf(stderr, "Get failed: %s\n", db_strerror(ret));
        return -1;
    }
    
    acceptor_record * flat_rec = (acceptor_record *) data.data;
    
    return flat_rec->ballot;
}

