#include <unistd.h>
#include "multicast.h"
#include "libpaxos_priv.h"

multicast_sender*   ls;
multicast_sender*   ps;
multicast_receiver* ar;

typedef struct acceptor_record_t {
    int     iid;
    int     ballot;
    int     value_ballot;
    int     value_size;
    char*   value;
} acceptor_record;

#include "acceptor_disk_helpers.c"

static int acceptor_id;
static acceptor_record acceptor_array[ACCEPTOR_ARRAY_SIZE];
static int send_buffer_size;
static char send_buffer[MC_MSG_MAX_DATA_SIZE];


int acceptor_init(int id) {
    int i;
    
    char path_buffer[30];
    
    sprintf(path_buffer, "/tmp/acceptor_%d.bdb", id);
        
    acceptor_id = id;
    send_buffer_size = 0;
    
    LOG(DBG, ("Acceptor %d: Initializing permastorage\n", acceptor_id));
    i = disk_init_permastorage(path_buffer);
    if (i != 0) {
        printf("Permastorage initialization failed\n");
        exit(1);
    }

    LOG(DBG, ("Acceptor %d: Initializing multicast\n", acceptor_id));
    ls = multicast_sender_new(PAXOS_LEARNERS_NET);
    ps = multicast_sender_new(PAXOS_PROPOSERS_NET);
    ar = mcast_receiver_new(PAXOS_ACCEPTORS_NET);

    if(ls == NULL || ps == NULL || ar == NULL) {
        printf("Error: acceptor_init() failed\n");
        return -1;
    }

    for(i = 0; i < ACCEPTOR_ARRAY_SIZE; i++) {
        acceptor_array[i].iid = -1;
        acceptor_array[i].ballot = -1;
        acceptor_array[i].value_size = 0;
        acceptor_array[i].value = NULL;
    }
    LOG(VRB, ("Acceptor %d: init completed\n", acceptor_id));
    return 1;
}

int flush_send_buffer(multicast_sender* sender, int msg_type) {
    int msg_size;
    msg_size = send_buffer_size;
    send_buffer_size = 0;
    LOG(V_DBG, ("Flushing acceptor send buffer, message size: %d\n", msg_size));
    return multicast_send(sender, send_buffer, msg_size, msg_type);
}

int add_promise_to_send_buffer(acceptor_record* rec) {
    promise_msg* promise;
    promise_batch_msg* promise_batch;
    int promise_size = sizeof(promise_msg) + rec->value_size;

    promise_batch = (promise_batch_msg*)send_buffer;

    // promise doesn't fit in buffer
    if (promise_size + send_buffer_size > MC_MSG_MAX_DATA_SIZE) {
        disk_force_syncrony(PAXOS_PREPARE);
        flush_send_buffer(ps, PAXOS_PROMISE);
    }

    if (send_buffer_size == 0) {
        promise_batch->count = 0;
        promise_batch->acceptor_id = acceptor_id;
        send_buffer_size += sizeof(promise_batch_msg);
    }

    promise = (promise_msg*) &send_buffer[send_buffer_size];
    promise->iid = rec->iid;
    promise->ballot = rec->ballot;
    promise->value_ballot = rec->value_ballot;
    promise->value_size = rec->value_size;

    if (rec->value_size > 0) {
        memcpy(promise->value, rec->value, rec->value_size);
    }

    promise_batch->count++;
    send_buffer_size += promise_size;
    
    LOG(V_DBG, ("Promise for iid:%d added to buffer\n", promise->iid));

    return 1;
}

int handle_prepare(prepare_msg* msg) {
    int ret = 0;
    acceptor_record* rec;

    rec = &acceptor_array[GET_ACC_INDEX(msg->iid)];

    // 一个新的实例，移除原来的实例
    if (msg->iid > rec->iid) {
        rec->iid = msg->iid;
        rec->ballot = msg->ballot;
        rec->value_ballot = -1;
        rec->value_size = 0;
        if (rec->value != NULL) {
            free(rec->value);
        }
        rec->value = NULL;
        
        LOG(DBG, ("Promising for instance %d with ballot %d, never seen before\n", msg->iid, msg->ballot));
        disk_update_record(rec);
        return add_promise_to_send_buffer(rec);
    }

    // 之前这个实例已经存在
    if (msg->iid == rec->iid) {
        if (msg->ballot <= rec->ballot) {
            LOG(DBG, ("Ignoring prepare for instance %d with ballot %d, already promised to %d\n", msg->iid, msg->ballot, rec->ballot));
            return 1;
        }
        //新的投票高于之前的投票
        LOG(DBG, ("Promising for instance %d with ballot %d, previously promised to %d\n", msg->iid, msg->ballot, rec->ballot));
        rec->ballot = msg->ballot;
        disk_update_record(rec);
        return add_promise_to_send_buffer(rec);
    }

    // 实例id小于记录的实例id
    if (msg->iid < rec->iid) {
        rec = disk_lookup_record(msg->iid);
        if(rec == NULL) {
            // 在数据库中没有记录
            rec = malloc(sizeof(acceptor_record));
            rec->iid           = msg->iid;
            rec->ballot        = -1;
            rec->value_ballot  = -1;
            rec->value_size    = 0;
            rec->value = NULL;            
        }
        
        if(msg->ballot > rec->ballot) {
            rec->ballot = msg->ballot;
            disk_update_record(rec);
            ret = add_promise_to_send_buffer(rec);
            
        } else {
            LOG(DBG, ("Ignoring prepare for instance %d with ballot %d, already promised to %d [info from disk]\n", msg->iid, msg->ballot, rec->ballot));
            ret = 0;
        }
    }

    if(rec != NULL) {
        if(rec->value != NULL) {
            free(rec->value);
        }
        free(rec);
    }

    return ret;
}

int handle_prepare_batch(prepare_batch_msg* bmsg) {
    int i, offset = 0;
    prepare_msg* pmsg;

    send_buffer_size = 0;
    ((promise_batch_msg*)send_buffer)->count = 0;

    for(i = 0; i < bmsg->count; i++) {
        pmsg = (prepare_msg*) &bmsg->data[offset];
        handle_prepare(pmsg);
        offset += sizeof(prepare_msg);
    }

    if (((promise_batch_msg*)send_buffer)->count > 0) {
        disk_force_syncrony(PAXOS_PREPARE);
        flush_send_buffer(ps, PAXOS_PROMISE);
    }

    return 1;
}

void apply_accept(acceptor_record* rec, accept_msg* amsg) {    
    //更新记录
    rec->iid = amsg->iid;
    rec->ballot = amsg->ballot;
    rec->value_ballot = amsg->ballot;
    if (rec->value != NULL) {
        free(rec->value);
    }
    rec->value = malloc(amsg->value_size);
    memcpy(rec->value, amsg->value, amsg->value_size);
    rec->value_size = amsg->value_size;
    
    //写到数据库中
    disk_update_record(rec);
    disk_force_syncrony(PAXOS_ACCEPT);
    LOG(V_DBG, ("Accept for iid:%d applied\n", rec->iid));
}

int send_learn_message(acceptor_record* rec) {
    learn_msg* lm = (learn_msg*) send_buffer;
    send_buffer_size = sizeof(learn_msg);
    lm->acceptor_id = acceptor_id;
    lm->iid = rec->iid;
    lm->ballot = rec->ballot;
    lm->value_size = rec->value_size;
    memcpy(lm->value, rec->value, rec->value_size);
    send_buffer_size = sizeof(learn_msg) + rec->value_size;
    
    LOG(V_DBG, ("Sending learn for iid:%d\n", rec->iid));

    return flush_send_buffer(ls, PAXOS_LEARN);
}

int handle_accept(accept_msg* amsg) {

    int ret;
    acceptor_record* rec;

    //读取数据
    rec = &acceptor_array[GET_ACC_INDEX(amsg->iid)];

    //获取之前写入的记录
    if (amsg->iid == rec->iid) {    
        if(amsg->ballot >= rec->ballot) {
            LOG(DBG, ("Accepting value for instance %d with ballot %d\n", amsg->iid, amsg->ballot));
            apply_accept(rec, amsg);
            return send_learn_message(rec);
        } else {
            LOG(DBG, ("Ignoring accept for instance %d with ballot %d, already given to ballot %d\n", amsg->iid, amsg->ballot, rec->ballot));
        }
        return 1;
    }

    //在acceprot数组中没有
    if (amsg->iid > rec->iid) {
        LOG(DBG, ("Accepting value instance %d with ballot %d, never seen before\n", amsg->iid, amsg->ballot));
        apply_accept(rec, amsg);
        return send_learn_message(rec);
    }

    //已经在acceprot数组中存在
    if (amsg->iid < rec->iid) {
        rec = disk_lookup_record(amsg->iid);
        if (rec == NULL) {
            LOG(DBG, ("Accepting value instance %d with ballot %d, never seen before [info from disk]\n", amsg->iid, amsg->ballot));
            rec = malloc(sizeof(acceptor_record));
            rec->value = NULL;
            apply_accept(rec, amsg);
            ret = send_learn_message(rec);
            return ret;
        }
        
        if(amsg->ballot >= rec->ballot) {
            LOG(DBG, ("Accepting value instance %d with ballot %d [info from disk]\n", amsg->iid, amsg->ballot));
            apply_accept(rec, amsg);
            ret = send_learn_message(rec);
        } else {
            LOG(DBG, ("Ignoring accept for instance %d with ballot %d, already given to ballot %d [info from disk]\n", amsg->iid, amsg->ballot, rec->ballot));
            ret = 0;
        }
        
        if(rec->value != NULL) {
            free(rec->value);
        }
        free(rec);
        
        return ret;
    }
    
    return 0;
}

int handle_lsync(int instance_id) {

    acceptor_record* rec;
    //Lookup
    rec = &acceptor_array[GET_ACC_INDEX(instance_id)];

    //获取内存中的记录
    if (instance_id == rec->iid) {
        //接受一个数据
        if (rec->value != NULL) {
            LOG(DBG, ("Answering to lsync for instance %d\n", instance_id));
            return send_learn_message(rec);
        }
        //没有数据
        LOG(DBG, ("Ignoring lsync for instance %d, record present but no value accepted\n", instance_id));
        return 0;
    }
    
    if (instance_id < rec->iid) {
        rec = disk_lookup_record(instance_id);
        //如果存在发送给学习者
        if (rec != NULL) {
            LOG(DBG, ("Answering to lsync for instance %d [info from disk]\n", instance_id));
            send_learn_message(rec);
            free(rec->value);
            free(rec);
        } else {
            LOG(DBG, ("Ignoring lsync for instance %d, no value accepted [info from disk]\n", instance_id));            
        }
        
        return 0;        
    }

    LOG(DBG, ("Ignoring lsync for instance %d, no info is available\n", instance_id));
    return 0;
    
}

int handle_batch_lsync(learner_sync_msg* lsmsg) {
    int i;
    for(i = 0; i < lsmsg->count; i++) {
        handle_lsync(lsmsg->ids[i]);
    }
    return 1;
}

int handle_message(multicast_msg* msg) {

    switch(msg->type) {
        case PAXOS_PREPARE: {
            return handle_prepare_batch((prepare_batch_msg*) msg->data);
        }

        case PAXOS_ACCEPT: {
            return handle_accept((accept_msg*) msg->data);
        }

        case PAXOS_LSYNC: {
            return handle_batch_lsync((learner_sync_msg*) msg->data);
        }

        default: {
            printf("Unkown message type: %d.\n", msg->type);            
        }
        
    }
    return 1;
}

int acceptor_start() {
    multicast_msg* msg;

    while (1) {
        if ((msg = mcast_recv(ar)) != NULL) {
            if (handle_message(msg) == -1)
                return -1;
        }
    }
}

