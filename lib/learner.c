#include <unistd.h>
#include <pthread.h>
#include <time.h>
#include <sys/time.h>

#include "config.h"
#include "multicast.h"
#include "libpaxos_priv.h"

#include "event_counters.h"

typedef struct learner_record_t {
    int         iid;
    learn_msg*  learns[N_OF_ACCEPTORS];
    int         final_value_ballot;
    char*       final_value;
    int         final_value_size;
} learner_record;


static multicast_sender*    as;
static multicast_receiver*  lr;
static char lsend_buffer[MC_MSG_MAX_DATA_SIZE];

static pthread_t            lrn_thread;
static pthread_t            rtr_thread;
static pthread_mutex_t      learner_array_lock = PTHREAD_MUTEX_INITIALIZER;

static lrn_value_wrapper*   list_head;
static lrn_value_wrapper*   list_tail;
static pthread_mutex_t      values_list_lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t       values_delivered_cond = PTHREAD_COND_INITIALIZER;

static int highest_enqueued = -1;
static int highest_closed = -1;
static int highest_seen;
static learner_record learner_array[LEARNER_ARRAY_SIZE];
static int majority = -1;

static time_t lsync_timer;

#ifdef COUNT_PAXOS_EVENTS
long int learner_send_lsync = 0;
long int learner_handle_learn = 0;
long int learner_total_holes = 0;

void learner_print_event_counters() {
    printf("Event counters for learner\n");
    pthread_mutex_lock(&learner_array_lock);
    printf("learner_send_lsync: %ld\n", learner_send_lsync);
    printf("learner_handle_learn: %ld\n", learner_handle_learn);
    printf("learner_total_holes: %ld\n", learner_total_holes);
    pthread_mutex_unlock(&learner_array_lock);
}
#else
void learner_print_event_counters() {
    printf("Event counters are disabled!\n");
}
#endif

int is_closed(learner_record* rec) {
    return (rec->final_value != NULL);
}

int add_learn_to_record(learner_record* rec, learn_msg* lmsg) {
    learn_msg * old_learn, * new_learn;
    
    if(lmsg->acceptor_id < 0 || lmsg->acceptor_id >= N_OF_ACCEPTORS) {
        printf("Error: received acceptor_id:%d\n", lmsg->acceptor_id);
        return 0;
    }
    
    old_learn = rec->learns[lmsg->acceptor_id];
    
    //第一次学习这个实例的这个决策者
    if(old_learn == NULL) {
        LOG(DBG, ("Got first learn for instance:%d, acceptor:%d\n", rec->iid,  lmsg->acceptor_id));
        new_learn = malloc(sizeof(learn_msg) + lmsg->value_size);
        memcpy(new_learn, lmsg, (sizeof(learn_msg) + lmsg->value_size));
        rec->learns[lmsg->acceptor_id] = new_learn;
        return 1;
    }
    
    //投票小于之前的投票，丢弃
    if(old_learn->ballot >= lmsg->ballot) {
        LOG(DBG, ("Dropping learn for instance:%d, more recent ballot already seen\n", rec->iid));
        return 0;
    }
    
    //有用的投票，重写
    LOG(DBG, ("Overwriting previous learn for instance %d\n", rec->iid));
    free(old_learn);
    new_learn = malloc(sizeof(learn_msg) + lmsg->value_size);
    memcpy(new_learn, lmsg, (sizeof(learn_msg) + lmsg->value_size));
    rec->learns[lmsg->acceptor_id] = new_learn;
    return 1;    
}


//returns 1 if message was relevant, 
//return 0 if message was already received
int update_record(learn_msg* lmsg) {
    int i;
    learner_record * rec;

    //已经学习过
    if(lmsg->iid >= highest_enqueued + LEARNER_ARRAY_SIZE) {
        LOG(DBG, ("Dropping learn for instance too far in future:%d\n", lmsg->iid));
        return 0;
    }

    //已经发送给
    if(lmsg->iid <= highest_enqueued) {
        LOG(DBG, ("Dropping learn for already enqueued instance:%d\n", lmsg->iid));
        return 0;
    }

    //取到这个实例的记录
    rec = &learner_array[GET_LEA_INDEX(lmsg->iid)];

    //这个实例的第一个消息
    if(rec->iid != lmsg->iid) {
        LOG(DBG, ("Received first message for instance:%d\n", lmsg->iid));

        //Clean record
        rec->iid = lmsg->iid;
        if (rec->final_value != NULL) 
            free(rec->final_value);
        rec->final_value = NULL;
        rec->final_value_size = -1;
        for(i = 0; i < N_OF_ACCEPTORS; i++) {
            if(rec->learns[i] != NULL) free(rec->learns[i]);
            rec->learns[i] = NULL;
        }
        //Add
        return add_learn_to_record(rec, lmsg);

    //获取记录的信息
    } else {
        if(is_closed(rec)) {
            LOG(DBG, ("Dropping learn for closed instance:%d\n", lmsg->iid));
            return 0;
        }
        //添加到学习的记录中
        return add_learn_to_record(rec, lmsg);
    }
}


//判断是不是大多数
int check_majority(learn_msg* lmsg) {
    int i, count = 0;
    learn_msg* stored_learn;
    learner_record* rec;
    
    rec = &learner_array[GET_LEA_INDEX(lmsg->iid)];

    for(i = 0; i < N_OF_ACCEPTORS; i++) {
        stored_learn = rec->learns[i];

        //没有值
        if(stored_learn == NULL) 
            continue;

        //值不同     
        if(stored_learn->value_size != lmsg->value_size) 
            continue;
            
        //值相同
        if(!memcmp(stored_learn->value, lmsg->value, lmsg->value_size))
            count++;
        
        //如果大于半数
        if (count >= majority) {
            LOG(DBG, ("Reached majority, instance %d is now closed!\n", rec->iid));

            rec->final_value_ballot = lmsg->ballot;
            rec->final_value_size = lmsg->value_size;
            rec->final_value = malloc(lmsg->value_size);
            memcpy(rec->final_value, lmsg->value, lmsg->value_size);
            
            //更新highest_closed
            if(lmsg->iid > highest_closed) 
                highest_closed = lmsg->iid;
                
            return 1;
        }
    }
    
    return 0;
}

void enqueue_values(int instance_id) {

    pthread_mutex_lock(&values_list_lock);
    
    while(1) {
        learner_record* rec = &learner_array[GET_LEA_INDEX(instance_id)];  

        if(!is_closed(rec)) break;

        LOG(DBG, ("Instance %d enqueued for client\n", instance_id));
        lrn_value_wrapper* w = malloc(sizeof(lrn_value_wrapper));
        w->iid = instance_id;
        w->ballot = rec->final_value_ballot;
        w->value = rec->final_value;
        w->value_size = rec->final_value_size;
        w->next = NULL;

        rec->final_value = NULL;

        if(list_head == NULL && list_tail == NULL) {
            list_head = w;
            list_tail = w;
            pthread_cond_signal(&values_delivered_cond);
        } else if (list_head != NULL && list_tail != NULL) {
            list_tail->next = w;
            list_tail = w;
        }
        
        highest_enqueued = instance_id;
        instance_id++;
    }
    
    pthread_mutex_unlock(&values_list_lock);

}

int handle_learn(learn_msg* lmsg) {
    EVENT_COUNT(learner_handle_learn++);
    
    if (lmsg->iid > highest_seen) {
        highest_seen = lmsg->iid;
    }
    int relevant = update_record(lmsg);
    if(!relevant) {
        LOG(DBG, ("Learner discarding learn for instance %d\n", lmsg->iid));
        return 0;
    }

    int got_majority = check_majority(lmsg);
    if(!got_majority) {
        LOG(DBG, ("Not yet a majority for instance %d\n", lmsg->iid));
        return 0;
    }

    if (lmsg->iid == highest_enqueued+1) {
        enqueue_values(lmsg->iid);
    }
    
    return 0;
}


void ask_retransmission() {
    int i;
    learner_record* rec;
    learner_sync_msg* lsync = (learner_sync_msg*) lsend_buffer;
    lsync->count = 0;
    int buffer_size = sizeof(learner_sync_msg);

    for(i = highest_enqueued+1; i < highest_seen; i++) {
        rec = &learner_array[GET_LEA_INDEX(i)];
        
        //没关闭或从未见过，请求同步
        if((rec->iid == i && !is_closed(rec)) || rec->iid < i) {
            LOG(V_DBG, ("Adding %d to next lsync message\n", i));
            lsync->ids[lsync->count] = i;
            EVENT_COUNT(learner_total_holes++);
            lsync->count++;
            buffer_size += sizeof(int);
            
            if((MC_MSG_MAX_DATA_SIZE - buffer_size) < sizeof(int)) {
                EVENT_COUNT(learner_send_lsync++);
                multicast_send(as, lsend_buffer, buffer_size, PAXOS_LSYNC);
                LOG(V_VRB, ("Requested %d lsyncs from %d to %d\n", lsync->count,  highest_enqueued+1, highest_seen));
                lsync->count = 0;
                buffer_size = sizeof(learner_sync_msg);
            }
        }
    }
    
    if(lsync->count > 0) {
        EVENT_COUNT(learner_send_lsync++);
        multicast_send(as, lsend_buffer, buffer_size, PAXOS_LSYNC);
                LOG(V_VRB, ("Requested %d lsyncs from %d to %d\n", lsync->count,  highest_enqueued+1, highest_seen));
    }
}


void* learner_loop(void* arg) {
    multicast_msg * msg;
    while(1) {
        if((msg = mcast_recv(lr)) != NULL) {
            if(msg->type != PAXOS_LEARN) {
                LOG(VRB, ("Learner ignoring message of type %d\n", msg->type));
                continue;                
            }
            
            pthread_mutex_lock(&learner_array_lock);
            handle_learn((learn_msg*) msg->data);
            pthread_mutex_unlock(&learner_array_lock);
        }
    }
}

void* retransmission_loop(void* arg) {
        
    struct timespec ts;
    struct timeval tv;
    pthread_cond_t dummy_cond = PTHREAD_COND_INITIALIZER;

    pthread_mutex_lock(&learner_array_lock);        
    while(1) {
        gettimeofday(&tv, NULL);        
        ts.tv_sec = tv.tv_sec;
        ts.tv_nsec = (LEARNER_LSYNC_INTERVAL + tv.tv_usec) * 1000;
                
        pthread_cond_timedwait(&dummy_cond, &learner_array_lock, &ts);        
        
        if(highest_seen > highest_enqueued) {
            ask_retransmission();
        }
    }
        pthread_mutex_unlock(&learner_array_lock);
}


int learner_init() {
    int i;
    learner_record* l;
    
    lsync_timer = time(NULL);
    
    list_head = NULL;
    list_tail = NULL;

    as = multicast_sender_new(PAXOS_ACCEPTORS_NET);
    lr = mcast_receiver_new(PAXOS_LEARNERS_NET);

    if(as == NULL || lr == NULL) {
        printf("Error: learner_init() failed\n");
        return -1;
    }

    majority = ((int)(N_OF_ACCEPTORS/2))+1;
    LOG(DBG, ("Learner starting, acceptors:%d, majority:%d\n", N_OF_ACCEPTORS, majority));

    memset(learner_array, '\0', (LEARNER_ARRAY_SIZE * sizeof(learner_record)));
    for(i = 0; i < LEARNER_ARRAY_SIZE; i++) {
        l = &learner_array[i];
        l->iid = -1;
    }

    if (pthread_create(&lrn_thread, NULL, learner_loop, (void*) NULL) != 0) {
        perror("Failed to initialize learner thread");
        return -1;
    }
    
    if (pthread_create(&rtr_thread, NULL, retransmission_loop, (void*) NULL) != 0) {
        perror("Failed to initialize retransmission thread");
        return -1;
    }
    
    return 1;
}

int learner_get_next_value(char** valuep) {
    int size = 0;
    lrn_value_wrapper* w;
    
    *valuep = NULL;
    if ((w = learner_get_next_value_wrapper()) != NULL) {
        size  = w->value_size;
        *valuep = w->value;
        free(w);
    }
    
    return size;
}

int learner_get_next_value_nonblock(char** valuep) {
    int size = 0;
    lrn_value_wrapper* w;
    
    *valuep = NULL;
    if ((w = learner_get_next_value_wrapper_nonblock()) != NULL) {
        size  = w->value_size;
        *valuep = w->value;
        free(w);
    }
    
    return size;
}

lrn_value_wrapper * learner_get_next_value_wrapper_nonblock() {
    lrn_value_wrapper* w;
    
    pthread_mutex_lock(&values_list_lock);

    if(list_head == NULL && list_tail == NULL) {
        w = NULL;
    } else if (list_head == list_tail) {
        LOG(V_DBG, ("Returning only element in learned value list\n"));
        w = list_head;
        list_head = NULL;
        list_tail = NULL;
    } else {
        w = list_head;
        list_head = w->next;
    }
    
    pthread_mutex_unlock(&values_list_lock);
    
    return w;
}


lrn_value_wrapper * learner_get_next_value_wrapper() {
    lrn_value_wrapper* w;
    
    pthread_mutex_lock(&values_list_lock);

    while(list_head == NULL && list_tail == NULL) {
        pthread_cond_wait(&values_delivered_cond, &values_list_lock);
    }
    
    if (list_head == list_tail) {
        LOG(V_DBG, ("Returning only element in learned value list\n"));
        w = list_head;
        list_head = NULL;
        list_tail = NULL;
    } else {
        w = list_head;
        list_head = w->next;
    }
    
    pthread_mutex_unlock(&values_list_lock);
    
    return w;
}
