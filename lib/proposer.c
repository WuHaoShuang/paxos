#include <time.h>
#include <pthread.h>
#include <unistd.h>
#include <sys/time.h>
#include <assert.h>

#include "multicast.h"
#include "libpaxos_priv.h"

#include "event_counters.h"

typedef enum status_flag_t {
  p1_pending,
  p1_ready,
  p2_reserved,
  p2_pending,
  p2_accepted
} status_flag;

typedef struct proposer_record_t {
    int iid;
    int ballot;
    status_flag status;
    int promise_count;
    promise_msg* promises[N_OF_ACCEPTORS];
    promise_msg* reserved_promise;
} proposer_record;

typedef struct pending_promise_t {
    int from_iid;
    int to_iid;
    long unsigned timer;
    struct pending_promise_t* next;
} pending_promise;

typedef struct client_value_wrapper_t {
    int                             value_size;
    void*                           value;
    struct client_value_wrapper_t*  next;
} client_value_wrapper;

static proposer_record proposer_array[PROPOSER_ARRAY_SIZE];

static int p1_ready_count;
static int p1_pending_count;
static pending_promise* p1_pending_list_head;
static pending_promise* p1_pending_list_tail;

static client_value_wrapper* p2_pending_accept;
static long unsigned p2_pending_timer;

static pthread_t                prop_thread;
static pthread_t                timeout_thread;
static pthread_t                delivery_thread;
static pthread_mutex_t          proposer_main_lock = PTHREAD_MUTEX_INITIALIZER;

static client_value_wrapper*    client_list_head;
static client_value_wrapper*    client_list_tail;
static int                      client_list_size = 0;

static multicast_sender*    as;
static multicast_receiver*  pr;

static int highest_open;
static int current_iid;

static int prop_id;

static char psend_buffer[MC_MSG_MAX_DATA_SIZE];
static int send_buffer_size;

static int majority;

static deliver_function deliver_callback = NULL;

//i.e. 10 + 3 = 13, 
#define BALLOT_NEW (MAX_PROPOSERS + prop_id) 
//i.e. 13 + 10 = 23, 33, 43, ...
#define BALLOT_NEXT(lastBal) (lastBal + MAX_PROPOSERS)
void periodic_phase2_check();

#ifdef COUNT_PAXOS_EVENTS
long int proposer_verify_val = 0;
long int proposer_delivering = 0;
long int proposer_delivering_reserved = 0;
long int proposer_instance_stolen = 0;
long int proposer_p1_timedout = 0;
long int proposer_p2_timedout = 0;
long int proposer_handle_promise = 0;
long int proposer_send_prepare = 0;
long int proposer_send_accept = 0;
void proposer_print_event_counters() {
    printf("Event counters for proposer\n");
    
    pthread_mutex_lock(&proposer_main_lock);

    printf("proposer_verify_val: %ld\n", proposer_verify_val);
    printf("proposer_delivering: %ld\n", proposer_delivering);
    printf("proposer_delivering_reserved: %ld\n", proposer_delivering_reserved);
    printf("proposer_instance_stolen: %ld\n", proposer_instance_stolen);
    printf("proposer_p1_timedout: %ld\n", proposer_p1_timedout);
    printf("proposer_p2_timedout: %ld\n", proposer_p2_timedout);
    printf("proposer_handle_promise: %ld\n", proposer_handle_promise);
    printf("proposer_send_prepare: %ld\n", proposer_send_prepare);
    printf("proposer_send_accept: %ld\n", proposer_send_accept);

    pthread_mutex_unlock(&proposer_main_lock);

    learner_print_event_counters();
}
#else
void proposer_print_event_counters() {
    printf("Event counters are disabled!\n");
}
#endif

int proposer_queue_size() {
    pthread_mutex_lock(&proposer_main_lock);
    int c = 0;
    client_value_wrapper * cvw = client_list_head;
    while(cvw != NULL) {
        c++;
        cvw = cvw->next;
    }
    client_list_size = c;

    pthread_mutex_unlock(&proposer_main_lock);
    
    return c;
}

int proposer_submit_value(char* value, int size) {
    pthread_mutex_lock(&proposer_main_lock);
    
    nolock_proposer_submit_value(value, size);
    
    periodic_phase2_check();

    pthread_mutex_unlock(&proposer_main_lock);

    return 0;
}

int nolock_proposer_submit_value(char* value, int size) {
    client_value_wrapper* w;

    LOG(DBG, ("P: Submitting value of size %d to proposer\n", size));
    
    w = malloc(sizeof(client_value_wrapper));
    w->value = malloc(size);
    w->value_size = size;    
    w->next = NULL;
    
    memcpy(w->value, value, size);
    
    if(client_list_head == NULL) {
        assert(client_list_tail == NULL);
        client_list_head = w;
        client_list_tail = w;
    } else {
        assert(client_list_tail != NULL);
        client_list_tail->next = w;
        client_list_tail = w;
    }
    client_list_size++;

    periodic_phase2_check();

    return 0;
}

int lock_proposer_submit() {
    return pthread_mutex_lock(&proposer_main_lock);
}

int unlock_proposer_submit() {
    return pthread_mutex_unlock(&proposer_main_lock);
}


long unsigned get_time_millis() {
    struct timeval tv;
    struct timezone tz;
    gettimeofday(&tv, &tz);
    return ((tv.tv_sec - 1198368000) * 1000) + (tv.tv_usec / 1000);
}

int flush_send_buffer(multicast_sender* sender, int msg_type) {
    int msg_size;
    msg_size = send_buffer_size;
    send_buffer_size = 0;
    return multicast_send(sender, psend_buffer, msg_size, msg_type);
}

void add_to_p1_pending_list(int from_iid, int to_iid) {
    pending_promise* p;
    
    long unsigned msec = get_time_millis();
    
    p = malloc(sizeof(pending_promise));
    p->from_iid = from_iid;
    p->to_iid   = to_iid;
    p->timer    = msec;
    p->next     = NULL;
    
    if(p1_pending_list_head == NULL) {
        assert(p1_pending_list_tail == NULL);
        p1_pending_list_head = p;
        p1_pending_list_tail = p;
    } else {
        assert(p1_pending_list_tail != NULL);
        p1_pending_list_tail->next = p;
        p1_pending_list_tail = p;
    }
    
    LOG(V_DBG, ("P: Addedd P1 timeout from %d to %d\n", from_iid, to_iid));
    
}


void initialize_proposer_rec(proposer_record* rec, int iid, int ballot) {
    int i;

    rec->iid    = iid;
    rec->ballot = ballot;
    rec->status = p1_pending;
    rec->promise_count = 0;
    rec->reserved_promise = NULL;

    for(i = 0; i < N_OF_ACCEPTORS; i++) {
        if(rec->promises[i] != NULL) {
            free(rec->promises[i]);
            rec->promises[i] = NULL;
        }
    }
}

void prepare_new_instances() {
    int i;
    prepare_msg* msg;
    prepare_batch_msg* bmsg;
    proposer_record* rec;
        
    bmsg = (prepare_batch_msg*)psend_buffer;
    
    bmsg->count = 0;
    send_buffer_size = sizeof(prepare_batch_msg);
    
    int last_to_open = (PROPOSER_PREEXEC_WIN_SIZE - (p1_ready_count + p1_pending_count)) + highest_open;

    LOG(DBG, ("P: Proposer tries to open instances from %d to %d\n", highest_open+1, last_to_open));

    for(i = highest_open+1; i <= last_to_open; i++) {
        // 添加准备消息到send_buffer
        msg = (prepare_msg*)&psend_buffer[send_buffer_size];
        msg->iid = i;
        msg->ballot = BALLOT_NEW;
        send_buffer_size += sizeof(prepare_msg);
        bmsg->count++;
        if((MC_MSG_MAX_DATA_SIZE - send_buffer_size) < sizeof(prepare_msg)) {
            EVENT_COUNT(proposer_send_prepare++);
            flush_send_buffer(as, PAXOS_PREPARE);
            bmsg->count = 0;
            send_buffer_size = sizeof(prepare_batch_msg);
        }
        
        // 添加到 proposer_array
        rec = &proposer_array[GET_PRO_INDEX(i)];
        initialize_proposer_rec(rec, i, BALLOT_NEW);
    }
    
    if (bmsg->count > 0) {
        EVENT_COUNT(proposer_send_prepare++);
        flush_send_buffer(as, PAXOS_PREPARE);
    }
        
    add_to_p1_pending_list(highest_open+1, last_to_open);
    p1_pending_count += (last_to_open - highest_open);

    highest_open = last_to_open;
}

void check_expired_p1_range(int from, int to) {
    int i, lower_reopen = -1, higher_reopen = -1;
    prepare_msg* msg;
    prepare_batch_msg* bmsg = (prepare_batch_msg*)psend_buffer;
    proposer_record* rec;
    
        for(i = from; i <= to; i++) {
            rec = &proposer_array[GET_PRO_INDEX(i)];
            
            if(rec->iid != i || rec->status != p1_pending) {
                LOG(V_DBG, ("P: iid:%d is not p1_pending, skipped\n", rec->iid));
                continue;
            }

            if( i < current_iid) {
                LOG(V_DBG, ("P: iid:%d is already closed, skipped\n", i));
                continue;
            }
                   
            EVENT_COUNT(proposer_p1_timedout++);
            //提高投票
            initialize_proposer_rec(rec, i, BALLOT_NEXT(rec->ballot));

            //添加到send_buffer
            msg = (prepare_msg*)&psend_buffer[send_buffer_size];
            msg->iid = i;
            msg->ballot = rec->ballot;
            LOG(DBG, ("P: Raised ballot for prepare of instance %d\n", i));
            
            send_buffer_size += sizeof(prepare_msg);
            bmsg->count++;
            if((MC_MSG_MAX_DATA_SIZE - send_buffer_size) < sizeof(prepare_msg)) {
                EVENT_COUNT(proposer_send_prepare++);
                flush_send_buffer(as, PAXOS_PREPARE);
                bmsg->count = 0;
                send_buffer_size = sizeof(prepare_batch_msg);
            }
            
            if(lower_reopen == -1) {
                lower_reopen = i;
            }
            higher_reopen = i;
            
        }
        
        if (lower_reopen != -1) {
            add_to_p1_pending_list(lower_reopen, higher_reopen);
        }
}

pending_promise* pop_next_p1_timeout() {
    pending_promise* p;
    
    
    if(p1_pending_list_head == NULL && p1_pending_list_tail == NULL) {
        p = NULL;
    } else if (p1_pending_list_head == p1_pending_list_tail) {
        p = p1_pending_list_head;
        p1_pending_list_head = NULL;
        p1_pending_list_tail = NULL;
    } else {
        p = p1_pending_list_head;
        p1_pending_list_head = p1_pending_list_head->next;
    }
    
    return p;    
    
}

void push_next_p1_timeout(pending_promise* p) {

    
    if(p1_pending_list_head == NULL && p1_pending_list_tail == NULL) {
        p1_pending_list_head = p;
        p1_pending_list_tail = p;
    } else if (p1_pending_list_head == p1_pending_list_tail) {
        p = p1_pending_list_head;
        p1_pending_list_head = NULL;
        p1_pending_list_tail = NULL;
    } else {
        p->next = p1_pending_list_head;
        p1_pending_list_head = p;
    }
}


void check_p1_pending() {
    pending_promise* p;
    prepare_batch_msg* bmsg;
    
    bmsg = (prepare_batch_msg*)psend_buffer;
    
    bmsg->count = 0;
    send_buffer_size = sizeof(prepare_batch_msg);
    
    long unsigned now = get_time_millis();
    
    while((p = pop_next_p1_timeout()) != NULL) {
        
        if(p->timer + PROMISE_TIMEOUT > now) {
            LOG(V_DBG, ("P: Timer for next p1_pending is not yet expired\n"));
            break;
        }            
        
        check_expired_p1_range(p->from_iid, p->to_iid);

        free(p);


    }

    
    if(p != NULL) {
        push_next_p1_timeout(p);
    }
     
    if (bmsg->count > 0) {
        EVENT_COUNT(proposer_send_prepare++);
        flush_send_buffer(as, PAXOS_PREPARE);
    }
        
}


void resubmit_pending_accept() {
    
    if(client_list_head == NULL) {
        assert(client_list_tail == NULL);
        client_list_head = p2_pending_accept;
        client_list_tail = p2_pending_accept;
    } else {
        assert(client_list_tail != NULL);
        p2_pending_accept->next = client_list_head;
        client_list_head = p2_pending_accept;
    }
    client_list_size++; 
}

void check_p2_pending() {
    prepare_msg* msg;
    prepare_batch_msg* bmsg;
    proposer_record* rec;
    
    long unsigned now = get_time_millis();
    
    //没有过期
    if(p2_pending_timer + ACCEPT_TIMEOUT > now) {
        LOG(V_DBG, ("P: Accept timeout for instance %d is not expired yet\n", current_iid));
        return;
    }
    
    LOG(V_VRB, ("P:  ! Accept for instance %d timed out, restarts from phase1 !\n", current_iid)); 
    EVENT_COUNT(proposer_p2_timedout++);

    //过期重新添加到尾部
    resubmit_pending_accept();
    
    //以更高的票数重新执行第1阶段
    rec = &proposer_array[GET_PRO_INDEX(current_iid)];
    
    //清除旧的承诺并将状态设置为p1_pending
    initialize_proposer_rec(rec, current_iid, BALLOT_NEXT(rec->ballot));
    
    bmsg = (prepare_batch_msg*)psend_buffer;
    bmsg->count = 0;
    send_buffer_size = sizeof(prepare_batch_msg);

    msg = (prepare_msg*)&psend_buffer[send_buffer_size];
    msg->iid = current_iid;
    msg->ballot = rec->ballot;
    bmsg->count++;
    send_buffer_size += sizeof(prepare_msg);

    EVENT_COUNT(proposer_send_prepare++);
    flush_send_buffer(as, PAXOS_PREPARE);
    
    add_to_p1_pending_list(current_iid, current_iid);
    p1_pending_count++;

    p2_pending_accept = NULL;
}


void send_accept_message(proposer_record* rec, client_value_wrapper* w) {
    accept_msg* amsg;

    EVENT_COUNT(proposer_send_accept++);
    
    send_buffer_size = 0;
    amsg = (accept_msg*)psend_buffer;
    
    amsg->iid = rec->iid;
    amsg->ballot = rec->ballot;
    amsg->value_size = w->value_size;
    memcpy(amsg->value, w->value, w->value_size);
    send_buffer_size += (sizeof(accept_msg) + w->value_size);
    
    multicast_send(as, psend_buffer, send_buffer_size, PAXOS_ACCEPT);
}

void deliver_next_value(proposer_record* rec) {
    client_value_wrapper* w = NULL;
    
    if(client_list_head != NULL) {
        client_list_size--;
        w = client_list_head;
        client_list_head = w->next;
        if(client_list_tail == w)
            client_list_tail = NULL;
    }

    if (w == NULL)
        return;

    LOG(DBG, ("P: Delivering next value\n"));

    send_accept_message(rec, w);
    rec->status = p2_pending;
    p2_pending_accept = w;
    p2_pending_timer = get_time_millis();
    p1_ready_count--;
}

int remove_from_pending_if_matches(char* value, int val_size) {
    client_value_wrapper * cvw = client_list_head;
    if((cvw != NULL) && 
        (cvw->value_size == val_size) &&
        memcmp(cvw->value, value, val_size) == 0) {

        client_list_head = client_list_head->next;
        if(client_list_tail == cvw) {
            client_list_tail = NULL;
        }
        client_list_size--;
        free(cvw->value);
        free(cvw);
        return 1;
    }
    return 0;
}

void deliver_reserved_value(proposer_record* rec) {
    promise_msg* msg;
    client_value_wrapper* w;
            
    msg = rec->reserved_promise;
    if (remove_from_pending_if_matches(msg->value, msg->value_size)) {
        LOG(DBG, ("Value was pushed back in pending list, removing it\n"));
    }
    
    LOG(DBG, ("P: Delivering reserved value for instance %d\n", rec->iid));

    w = malloc(sizeof(client_value_wrapper));
    w->value = malloc(msg->value_size);
    w->value_size = msg->value_size;
    memcpy(w->value, msg->value, msg->value_size);
    
    send_accept_message(rec, w);
    rec->status = p2_pending;
    p2_pending_accept = w;
    p2_pending_timer = get_time_millis();
}

void verify_accepted_value(lrn_value_wrapper* lmsg) {
    //检查接受的值是否是我们发送的
    //如果不是刷新值
    
    if (lmsg->iid != current_iid) {
        printf("P: verify_accepted_value: learn msg contains iid %d while current iid is: %d", lmsg->iid, current_iid);
        exit(0);
    }

    current_iid++;
    
    //在列表中检查值是否为第一个，可能如果P2到期并从P1重新启动 但是值已经实际提交了
    if(remove_from_pending_if_matches(lmsg->value, lmsg->value_size)) {
        LOG(DBG, ("P: Value accepted, instance %d removing from client list head\n", (current_iid-1)));
        return;
    }
    
    //不是在等待学习消息
    if(p2_pending_accept == NULL) {
        LOG(DBG, ("P: Someone's value was learned for instance %d \n", (current_iid-1)));
        return;
    }

    if((lmsg->value_size != p2_pending_accept->value_size) || (memcmp(lmsg->value, p2_pending_accept->value, p2_pending_accept->value_size) != 0)) {
        //接受的是其他提议者的值
        LOG(DBG, ("P: A different value was learned for instance %d, re-queuing our value\n", (current_iid-1)));
        EVENT_COUNT(proposer_instance_stolen++);
        resubmit_pending_accept();
    } else {
        //当前提议者的值被接受
        LOG(DBG, ("P: Value accepted, instance %d\n", (current_iid-1)));
        free(p2_pending_accept->value);
        free(p2_pending_accept);
        p2_pending_accept = NULL;
    }
}

int handle_promise(promise_msg* pmsg, int acceptor_id, proposer_record* rec) {
    promise_msg* copy;
    
    //忽略，因为在提议数组中没有关于这个iid的信息
    if(rec->iid != pmsg->iid) {
        LOG(DBG, ("P: Promise for %d ignored, no info in array\n", pmsg->iid));
        return 0;
    }
    
    //这个承诺是不相关的，因为我们没有等待承诺这个iid。
    if(rec->status != p1_pending) {
        LOG(DBG, ("P: Promise for %d ignored, instance is not p1_pending\n", pmsg->iid));
        return 0;        
    }
    
    //这个承诺是旧的，或者属于另一个提议者
    if(rec->ballot != pmsg->ballot) {
        LOG(DBG, ("P: Promise for %d ignored, not our ballot\n", pmsg->iid));
        return 0;
    }
    
    //已经接受承诺
    if(rec->promises[acceptor_id] != NULL) {
        LOG(DBG, ("P: Promise for %d ignored, already received\n", pmsg->iid));
        return 0;
    }
    
    //接受承诺，保存在proposer_array中
    copy = malloc(sizeof(promise_msg) + pmsg->value_size);
    memcpy(copy, pmsg, sizeof(promise_msg) + pmsg->value_size);
    rec->promises[acceptor_id] = copy;
    rec->promise_count++;
    
    if(rec->promise_count < majority) {
        LOG(DBG, ("P: Promise for %d received, not a majority yet\n", pmsg->iid));
        return 0;
    }
        
    LOG(DBG, ("P: Promise for %d received, majority reached!\n", pmsg->iid));
    return 1;
}

void check_ready(proposer_record* rec) {
    int i, most_recent_ballot = -1;
    int most_recent_ballot_index = -1;
    
    promise_msg* pmsg;
    
    for(i = 0; i < N_OF_ACCEPTORS; i++) {
        pmsg = rec->promises[i];
        if(pmsg == NULL)
            continue;
        
        if(pmsg->value_size == 0)
            continue;
        
        if(pmsg->value_ballot > most_recent_ballot) {
            most_recent_ballot = pmsg->value_ballot;
            most_recent_ballot_index = i;
        }   
    }
    
    // 没有值
    if(most_recent_ballot_index == -1) {
        rec->status = p1_ready;
        p1_ready_count++;
        p1_pending_count--;
        LOG(DBG, ("P: Instance %d is now ready! (pen:%d read:%d)\n", rec->iid, p1_pending_count, p1_ready_count ));

        return;
    }
    
    //发送给决策者
    rec->status = p2_reserved;
    rec->reserved_promise = rec->promises[most_recent_ballot_index];
    LOG(DBG, ("P: Instance %d is now reserved!\n", rec->iid));
    p1_pending_count--;
}

void handle_promise_batch (promise_batch_msg* pb_msg) {
    int i, offset = 0;
    proposer_record* rec;
    promise_msg * pm;
    
    EVENT_COUNT(proposer_handle_promise++);

    for(i = 0; i < pb_msg->count; i++) {
        pm = (promise_msg *)&pb_msg->data[offset];
        rec = &proposer_array[GET_PRO_INDEX(pm->iid)];        
        
        if (handle_promise(pm, pb_msg->acceptor_id, rec) != 0) {
            check_ready(rec);
        }
        offset += (sizeof(promise_msg) + pm->value_size);
    }
}

void periodic_phase2_check() {
    proposer_record* rec;

    rec = &proposer_array[GET_PRO_INDEX(current_iid)];
    if (rec->iid == current_iid) {
        if (rec->status == p1_ready) {
                    
            EVENT_COUNT(proposer_delivering++);
            deliver_next_value(rec);            
                
        } else if (rec->status == p2_reserved) {         
            EVENT_COUNT(proposer_delivering_reserved++);
            deliver_reserved_value(rec);
            
        }            
    }
    if(p1_ready_count + p1_pending_count < (PROPOSER_PREEXEC_WIN_SIZE/2)) {
        
        prepare_new_instances();
    }
}

void* delivery_check_loop(void* arg) {

    lrn_value_wrapper* lmsg;

    while (1) {
        
        lmsg = NULL;
        lmsg = learner_get_next_value_wrapper();

        pthread_mutex_lock(&proposer_main_lock);
        
        verify_accepted_value(lmsg);
        
        EVENT_COUNT(proposer_verify_val++);
        
        periodic_phase2_check();
        
        pthread_mutex_unlock(&proposer_main_lock);
        
        if(deliver_callback != NULL) {
            deliver_callback(lmsg->value, lmsg->value_size);
        } 
        free(lmsg->value);
        free(lmsg);

    }
}

void* timeout_checks_loop(void* arg) {
    struct timespec ts;
    struct timeval tv;
    pthread_cond_t dummy_cond = PTHREAD_COND_INITIALIZER;
        
    int timeout_check_interval;
    
    if(PROMISE_TIMEOUT < ACCEPT_TIMEOUT) {
        timeout_check_interval = (PROMISE_TIMEOUT/2)*1000;
    } else {
        timeout_check_interval = (ACCEPT_TIMEOUT/2)*1000;
    }

    pthread_mutex_lock(&proposer_main_lock);

    while(1) {
        
        gettimeofday(&tv, NULL);        
        ts.tv_sec = tv.tv_sec;
        ts.tv_nsec = (timeout_check_interval + tv.tv_usec) * 1000;
                
        pthread_cond_timedwait(&dummy_cond, &proposer_main_lock, &ts);
        if (p1_pending_count > 0) {
            check_p1_pending();
        }

        if (p2_pending_accept != NULL) {
            check_p2_pending();
        }

        if(p1_ready_count + p1_pending_count < (PROPOSER_PREEXEC_WIN_SIZE/2)) {
            prepare_new_instances();
        }

    }
        pthread_mutex_unlock(&proposer_main_lock);        
}

void* proposer_loop(void* arg) {
    multicast_msg * mmsg;

    while(1) {
        mmsg = mcast_recv(pr);
        
        if(mmsg->type != PAXOS_PROMISE) {
            printf("P: Proposer ignoring message of type %d\n", mmsg->type);
        } else {
            pthread_mutex_lock(&proposer_main_lock);
            handle_promise_batch((promise_batch_msg*) mmsg->data);
            
            periodic_phase2_check();
            
            if(p1_ready_count + p1_pending_count < (PROPOSER_PREEXEC_WIN_SIZE/2)) {
                prepare_new_instances();
            }

            pthread_mutex_unlock(&proposer_main_lock);
        }
    }
}

int proposer_init(int proposer_id) {
    
    p1_ready_count = 0;
    p1_pending_count = 0;
    p1_pending_list_head = NULL;
    p1_pending_list_tail = NULL;
    
    p2_pending_accept = NULL;
   
    client_list_head = NULL;
    client_list_tail = NULL;
    
    highest_open = -1;
    current_iid = 0; 
    prop_id = proposer_id;
    
    send_buffer_size = 0;
    
    majority = ((int)N_OF_ACCEPTORS/2) + 1;
    
    if (learner_init() < 0) {
        printf("P: Error: proposer_init() failed\n");
        return -1;    
    }
    
    as = multicast_sender_new(PAXOS_ACCEPTORS_NET);
    pr = mcast_receiver_new(PAXOS_PROPOSERS_NET);
    
    if(as == NULL || pr == NULL) {
        printf("P: Error: proposer_init() failed\n");
        return -1;
    }
    
    if (pthread_create(&prop_thread, NULL, proposer_loop, (void*) NULL) != 0) {
        perror("P: Failed to initialize proposer thread");
        return -1;
    }

    if (pthread_create(&timeout_thread, NULL, timeout_checks_loop, (void*) NULL) != 0) {
        perror("P: Failed to initialize timeouts thread");
        return -1;
    }

    if (pthread_create(&delivery_thread, NULL, delivery_check_loop, (void*) NULL) != 0) {
        perror("P: Failed to initialize learner thread");
        return -1;
    }    
    return 1;
    
    LOG(VRB, ("P: Proposer %d init completed\n", prop_id));
}

int proposer_init_and_deliver(int proposer_id, deliver_function f) {
    deliver_callback = f;
    return proposer_init(proposer_id);
}
