LIBPAXOS	= libpaxos.a
MODULES 	= common lib tests
AR			= ar
ARFLAGS		= rc
QUIET		= @

BDB_DIR		= $(HOME)/bdb/build_unix/


.PHONY: $(MODULES) $(BDB_DIR)

all: $(LIBPAXOS) $(MODULES)
	@echo "Done."

$(MODULES):	
	$(QUIET) $(MAKE) QUIET=$(QUIET) BDB_DIR=$(BDB_DIR) --directory=$@ || exit 1;
	
$(LIBPAXOS): $(BDB_DIR) common lib 
	$(QUIET) $(AR) $(ARFLAGS) $@ $(addsuffix /*.o, $^)
	$(QUIET) ranlib $@
	@echo "Libpaxos done."
	
BDB_ERROR_MSG="Warning: Berkeley DB was not found!! BDB is required for the acceptor permanent storage, please download and build BDB and update the BDB_DIR variable in Makefile"
$(BDB_DIR):
	$(QUIET) test -e $(BDB_DIR) || \
	( echo "$(BDB_ERROR_MSG)" && exit 1 )

clean:
	$(QUIET) rm -f $(LIBPAXOS)
	$(QUIET) for m in $(MODULES); do \
		make clean --directory=$$m; \
	done
	
