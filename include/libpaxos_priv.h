#ifndef _LIBPAXOS_PRIV_H_
#define _LIBPAXOS_PRIV_H_

#include "libpaxos.h"
#include "config.h"

#define PAXOS_PREPARE   1
#define PAXOS_ACCEPT    2
#define PAXOS_LSYNC     3
#define PAXOS_PROMISE   4
#define PAXOS_LEARN     5



typedef struct prepare_msg_t {
    int iid;
    int ballot;
} prepare_msg;

typedef struct prepare_batch_msg_t {
    int     count;
    char    data[0];
} prepare_batch_msg;

typedef struct promise_msg_t {
    int     iid;
    int     ballot;
    int     value_ballot;
    int     value_size;
    char    value[0];
} promise_msg;

typedef struct promise_batch_msg_t {
    int     acceptor_id;
    int     count;
    char    data[0];
} promise_batch_msg;

typedef struct accept_msg_t {
    int     iid;
    int     ballot;
    int     value_size;
    char    value[0];
} accept_msg;

typedef struct learn_msg_t {
    int     acceptor_id;
    int     iid;
    int     ballot;
    int     value_size;
    char    value[0];
} learn_msg;

typedef struct learner_sync_msg_t {
    int     count;
    int     ids[0];
} learner_sync_msg;


typedef struct lrn_value_wrapper_t {
    int     iid;
    int     ballot;
    int     value_size;
    void*   value;
    struct lrn_value_wrapper_t* next;
} lrn_value_wrapper;

lrn_value_wrapper * learner_get_next_value_wrapper();
lrn_value_wrapper * learner_get_next_value_wrapper_nonblock();


#define GET_ACC_INDEX(n) (n & (ACCEPTOR_ARRAY_SIZE-1))

#define GET_LEA_INDEX(n) (n & (LEARNER_ARRAY_SIZE-1))


#define GET_PRO_INDEX(n) (n & (PROPOSER_ARRAY_SIZE-1))


#define VRB 1
#define V_VRB 2
#define DBG 3
#define V_DBG 4

#define LOG(L, S) if(VERBOSITY_LEVEL >= L) printf S

#endif

