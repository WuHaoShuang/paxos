#ifndef EVENT_COUNTERS_H_GKHDRYF2
#define EVENT_COUNTERS_H_GKHDRYF2

#ifdef COUNT_PAXOS_EVENTS

#define EVENT_COUNT(stmt) { stmt; }

#else

#define EVENT_COUNT(disabled) {}

#endif


#endif
