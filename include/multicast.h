#ifndef _MULTICAST_H_
#define _MULTICAST_H_

#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

//Set to 1 to activate, prints info about enqueuing and dequeuing of messages in the multicast receiver queues
#define DEBUG_RECV_THREAD 0 

#define MC_ADDR_STRING_SIZE 16

typedef struct multicast_msg_t {
    int size; //Size of 'data' in bytes
    int type;
    char data[0];
} multicast_msg;

#define MULTICAST_MSG_SIZE(m) (sizeof(multicast_msg) + m->size)
#define MC_MSG_MAX_DATA_SIZE (9000 - sizeof(multicast_msg))

typedef struct mc_msg_wrapper_t {
    multicast_msg *             msg;
    struct mc_msg_wrapper_t *   next;
} mc_msg_wrapper;

typedef struct multicast_receiver_t {
	int                         sock;
	socklen_t                   addrlen;
	struct sockaddr_in          addr;
	char                        addr_string[MC_ADDR_STRING_SIZE];
	int                         port;
	
    // struct ip_mreq              mreq;
	mc_msg_wrapper *            list_head;
	mc_msg_wrapper *            list_tail;
	pthread_mutex_t             list_mutex;

	multicast_msg *             recv_buffer;
	pthread_t                   recv_thread;	
} multicast_receiver;


typedef struct multicast_sender_t {
	int                         sock;
	socklen_t                   addrlen;
	struct                      sockaddr_in addr;
	char                        addr_string[MC_ADDR_STRING_SIZE];
	int                         port;
	multicast_msg *         send_buffer;
} multicast_sender;

multicast_sender * multicast_sender_new (char * group, int port);

int multicast_send (multicast_sender * sender, char * message, size_t msg_size, int msg_type);

multicast_receiver * multicast_receiver_new (char * group, int port);

multicast_msg * multicast_recv (multicast_receiver * receiver);

multicast_receiver * mcast_receiver_new (char * group, int port);
multicast_msg * mcast_recv (multicast_receiver * receiver);

void print_receiver (multicast_receiver * receiver);
void print_sender (multicast_sender * sender);

#endif /* _MULTICAST_H_ */
