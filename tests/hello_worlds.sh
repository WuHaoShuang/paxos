#!/bin/bash

# Check dir
DIR=`pwd`;
if [ `basename $DIR` != "tests" ]; then
    echo "Please run this script from tests/"
	exit
fi

# Parametrize output files
OUT_S="$DIR/hw_sender.txt"
OUT_R="$DIR/hw_receiver.txt"
OUT_DIFF="$DIR/hw_diff.txt"

# Clear previous test results
rm -rf $OUT_S $OUT_R $OUT_DIFF

# Start and detach process, save pid
echo "Starting receiver"
./hello_worlds -l "$OUT_R" > /dev/null &
# X_PID=$!;

sleep 1

# Start and detach process, save pid
echo "Starting sender"
./hello_worlds -s "$OUT_S" > /dev/null &
# X_PID=$!;
# EXITVAL=$?

# Wait for test termination
sleep 3

# Kill Processes
KILL_FILE=hw1.kill
# Kill all test and verify that they were already completed
killall hello_worlds &> $KILL_FILE
K_COUNT=`cat $KILL_FILE |grep "No matching" | wc -l`
rm -rf $KILL_FILE
let K_COUNT=K_COUNT+0

# Check results
# Return $SUCCESS or $FAILURE

#check that test processes terminated
if [  $K_COUNT -lt 1 ]; then
	echo "Some test process did not terminate!"
	exit $FAILURE
fi

if [ ! -e "$OUT_S" ]; then
	echo "Sender file not found!"
	exit $FAILURE
fi

#check sender file exists and is not empty
S_LINES=`cat $OUT_S | wc -l`
let S_LINES=S_LINES+0
echo "sender file: $S_LINES lines"
if [[ $S_LINES == 0 ]]; then
	echo "Sender file is empty!"
	exit $FAILURE
fi

#check receiver file exists and is not empty
if [ ! -e "$OUT_R" ]; then
	echo "Receiver file not found!"
	exit $FAILURE
fi
R_LINES=`cat $OUT_R | wc -l`; let R_LINES=R_LINES+0
echo "receiver file: $R_LINES lines"
if [[ $R_LINES -lt 1 ]]; then
	echo "Receiver file is empty!"
	exit $FAILURE
fi

#check differences among files
diff $OUT_S $OUT_R > $OUT_DIFF
D_LINES=`cat $OUT_DIFF | wc -l`; let D_LINES=D_LINES+0
if [[ $D_LINES == 0 ]]; then
	echo "No differences"
	exit $SUCCESS
else
	echo "There are differences between sender and receiver output! ($D_LINES lines)"
	echo "Check $OUT_DIFF"
	exit $FAILURE	
fi

echo "Unknow exit status: $EXITVAL"
exit $FAILURE


