#include <stdio.h>

#include "db.h"
DB *acc_db; 

int main (int argc, char const *argv[])
{
    
    char* acc_db_name = "mydb.bdb";
    acc_db = NULL;
    
    u_int32_t open_flags;
    int ret;
    
    //Removes the previous DB if there
    remove(acc_db_name);

    /* Initialize the DB handle */
    ret = db_create(&acc_db, NULL, 0);
    if (ret != 0) {
        fprintf(stderr, "Create: %s: %s\n", acc_db_name, db_strerror(ret));
        return(ret);
    }
                                                                                                                               
    /* Set up error handling for this database */
    acc_db->set_errfile(acc_db, stdout);
    acc_db->set_errpfx(acc_db, "program_name");

    /* Set the open flags */
    open_flags = DB_CREATE;

    /* Now open the database */
    ret = acc_db->open(acc_db,        /* Pointer to the database */
                    NULL,       /* Txn pointer */
                    acc_db_name,  /* File name */
                    NULL,       /* Logical db name (unneeded) */
                    DB_BTREE,   /* Database type (using btree) */
                    open_flags, /* Open flags */
                    0);         /* File mode. Using defaults */
    if (ret != 0) {
        acc_db->err(acc_db, ret, "Database '%s' open failed.", acc_db_name);
        return(ret);
    }
    printf("init ok\n");
    
    
    return 0;
}