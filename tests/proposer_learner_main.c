#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>     
#include <sys/types.h>
#include <sys/wait.h>

#include "libpaxos.h"
#include "libpaxos_priv.h"

static int proposer_id = 0;
static int wait_interval = 100; //0.1 Seconds. this is in MILLIeconds. 
static FILE* values_file = NULL;
static char* filename = NULL;
static char* exit_after = "10";


#define MAX_VAL_SIZE PAXOS_MAX_VALUE_SIZE
static char val_buffer[MAX_VAL_SIZE];


void handle_cltr_c (int sig) {
	LOG (VRB, ("Caught exit signal\n"));
	if(strcmp(filename, "stdin") != 0) {
        fclose(values_file);
	}
    exit (0);
}

void pusage() {
    printf("-i N    : starts proposer with id N (default is 0)\n");
    printf("-w N    : waits N milliseconds between each proposal (default is 100)\n");
    printf("-f FILE : reads values to propose as lines from FILE (default is stdin");
    printf("-e N    : exits after N values were learned");

    printf("-h   : prints this message and exits\n");
}

void parse_args(int argc, char * const argv[]) {
    int c;
    while((c = getopt(argc, argv, "i:w:f:e:h")) != -1) {
        switch(c) {            
            case 'i': {
                proposer_id = atoi(optarg);
            }
            break;

            case 'w': {
                wait_interval = atoi(optarg);
            }
            break;

            case 'f': {
                filename = optarg;
            }
            break;

            case 'e': {
                exit_after = optarg;
            }
            break;
            
            case 'h':
            default: {
                pusage();
                exit(0);
            }
        }
    }
    
    if (filename == NULL) {
        filename = "stdin";
        values_file = stdin;
    } else {
        values_file = fopen(filename, "r");
        if(values_file == NULL) {
            perror("Could not open values file");
            exit(1);
        }
    }
}

void propose_values_from_file() {
    int microseconds = wait_interval*1000;
    int val_size;
    while(1) {
        if(fgets(val_buffer, MAX_VAL_SIZE, values_file) == NULL) {
            if(feof(values_file)) {
                printf("All values submitted");
                return;
            } else {
                perror("Reading values file");
                exit(1);
            }
        }
     
        val_size = strlen(val_buffer);
        
        // Replace final '\n' with '\0'
        val_buffer[val_size-1] = '\0';
        printf("Submitting \"%s\" [s:%d]\n", val_buffer, val_size);
        
        proposer_submit_value(val_buffer, val_size);
        usleep(microseconds);
    }
}

int main (int argc, char const *argv[]) {    
    signal (SIGINT, handle_cltr_c);
    
    parse_args(argc, (char **)argv);

    printf("Proposer_id is:%d\n", proposer_id);
    printf("Will wait %d milliseconds between submits\n", wait_interval);
    printf("Will read values from %s\n", filename);
    
    if (proposer_init(proposer_id) == -1) {
        printf("Failed to intialize proposer %d!\n", proposer_id);
        exit(1);
    }
    
    printf("All values submitted, waiting to learn them\n");
    
    //fork learner process  
	int pid = fork();
	if (pid < 0) {
		//error occurred
		fprintf(stderr, "Fork Failed\n"); 
	}
	else if (pid == 0) { 
		//child process
        execl("./learner_main", "learner_main", "-k", exit_after, (char *) 0);
        
	} 
	else { 
		//parent process	
        propose_values_from_file();
		wait (NULL); 
	}
        
    
    return 0;
}

