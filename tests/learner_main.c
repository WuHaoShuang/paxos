#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include "libpaxos.h"

int exit_after = 100;
int print_multiple = 1;
int mute = 0;
                
void pusage() {
    printf("-k N : Exits after learning N values\n");
    printf("-p N : Prints updates every N messages (default:1 = print all)\n");
    printf("-m   : Mute (overrides -p)\n");
    printf("-h   : prints this message\n");
}

void parse_args(int argc, char * const argv[]) {
    int c;
    while((c = getopt(argc, argv, "k:p:m")) != -1) {
        switch(c) {
            case 'k': {
                exit_after = atoi(optarg);
            }
            break;

            case 'p': {
                print_multiple = atoi(optarg);
            }
            break;

            case 'm': {
                mute = 1;
            }
            break;
            
            case 'h':
            default: {
                pusage();
                exit(0);
            }
        }
    }
}


int main (int argc, char const *argv[]) {
    int learn_count = 0;
    
    parse_args(argc, (char **)argv);

    if (learner_init() == -1) {
        printf("Learner init failed.\n");
        exit(1);
    }
    
    char * v;
    int val_size;
    while(1) {
        val_size = learner_get_next_value(&v);
        if(!mute && ((learn_count % print_multiple) == 0)) {
            printf("LEARNED_VAL:%d (size:%d) %s\n", learn_count, val_size, v);
        }
        free(v);
        learn_count++;

        if(learn_count >= exit_after) {
            printf("%d values learned\n", learn_count);
            learner_print_event_counters();
            exit(0);
        }
    }
    return 0;
}

