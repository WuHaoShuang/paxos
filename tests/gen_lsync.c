#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "multicast.h"
#include "libpaxos_priv.h"

void pusage() {
    printf("-n N : sends lsyncs for N instances \n");
    printf("-f N : sends lsyncs starting from instance N and incrementing\n");

    printf("-h   : prints this message and exits\n");
}


int n_of_lsync = 1;
int first_instance_id = 0;

void parse_args(int argc, char * const argv[]) {
    int c;
    while((c = getopt(argc, argv, "n:f:h")) != -1) {
        switch(c) {
            case 'n': {
                n_of_lsync = atoi(optarg);
            }
            break;
            
            case 'f': {
                first_instance_id = atoi(optarg);
            }
            break;
            
            case 'h':
            default: {
                pusage();
                exit(0);
            }
        }
    }    
}

char send_buffer[MC_MSG_MAX_DATA_SIZE];

void gen_and_send() {
    multicast_sender* s = multicast_sender_new(PAXOS_ACCEPTORS_NET);
    learner_sync_msg * lsync;
    
    if (s == NULL) {
        printf("Failed to initialize sender. Exit.\n");
        exit(0);
    }

    int i, last_id, sres;
    lsync = (learner_sync_msg *)send_buffer;

    lsync->count = 0;
    last_id = first_instance_id + n_of_lsync;
    for(i = first_instance_id; i < last_id; i++) {
        lsync->ids[lsync->count] = i;
        lsync->count++;
    }
    
    sres = multicast_send(s, send_buffer, (sizeof(learner_sync_msg) + (sizeof(int) * lsync->count)), PAXOS_LSYNC);
    if(sres == -1) {
        printf("Multicast send error. Exit\n");
        exit(0);
    }

}

int main (int argc, char const *argv[]) {
    parse_args(argc, (char **)argv);
    
    printf("Will request sync for %d prepares from iid:%d to iid:%d\n", n_of_lsync, first_instance_id, (n_of_lsync + first_instance_id));
    
    gen_and_send();
    return 0;
}

