#!/bin/bash

# Check dir
DIR=`pwd`;
if [ `basename $DIR` != "tests" ]; then
    echo "Please run this script from tests/"
	exit
fi
VALUES=100

# xterm -geometry 200x24+10+10 -e "./packet_logger -n &> ~/Desktop/net.txt" &

xterm -geometry 80x24+10+500 -e "./learner_main -k $VALUES -p 1; echo Press enter to close this window && read" &
sleep 1

xterm -geometry 80x24+10+250 -e "./acceptor_main -i 0; echo Press enter to close this window && read" &
sleep 1

xterm -geometry 80x24+100+250 -e "./acceptor_main -i 1; echo Press enter to close this window && read" &
sleep 1

xterm -geometry 80x24+200+250 -e "./acceptor_main -i 2; echo Press enter to close this window && read" &
sleep 1

./proposer_maxspeed -i 0 -v $VALUES -w 100000 -m 30 -M 3000 -s 1234 -q300 


