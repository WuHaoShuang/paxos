#!/bin/bash

# Check dir
DIR=`pwd`;
if [ `basename $DIR` != "tests" ]; then
    echo "Please run this script from tests/"
	exit
fi

# N_OF_ACCEPTORS=`grep "define N_OF_ACCEPTORS" ../config.h | awk '{print $3}'`
# let N_OF_ACCEPTORS=N_OF_ACCEPTORS+0;
# echo "libpaxos is configured for $N_OF_ACCEPTORS acceptors"
# if [ $N_OF_ACCEPTORS -lt 3 ]; then
#     echo "We need at least 3 acceptors for this test"
# 	exit
# fi
# let HALF=(N_OF_ACCEPTORS/2)
# let LAST_ACC_ID=(N_OF_ACCEPTORS - 1)

# Parametrize output files
OUT_NET="$DIR/lt_log3.txt"
OUT_LEA="$DIR/lt_lea3.txt"
OUT_CMP="$DIR/lt_cmp3.txt"
OUT_DIFF="$DIR/lt_diff3.txt"

# Clear previous test results
rm -rf $OUT_NET $OUT_LEA $OUT_CMP $OUT_DIFF

# Start and detach process, save pid
echo "Starting network logger"
./packet_logger -f $OUT_NET > /dev/null &
NET_PID=$!;

# Start and detach process, save pid
echo "Starting learner"
./learner_main -k 6 > $OUT_LEA &
# LEA_PID=$!;

VAL0="062342346786"
echo $VAL0 > $OUT_CMP
VAL1="1123123112"
echo $VAL1 >> $OUT_CMP
VAL2="2344563534"
echo $VAL2 >> $OUT_CMP
VAL3="345645646"
echo $VAL3 >> $OUT_CMP
VAL4="4534234234"
echo $VAL4 >> $OUT_CMP
VAL5="57867456"
echo $VAL5 >> $OUT_CMP

echo "Sending learns for 0"
./gen_learns -i 0 -v $VAL0 > /dev/null
sleep 1

echo "Sending learns for 1"
./gen_learns -i 1 -v $VAL1 > /dev/null
sleep 1

echo "Sending learns for 4"
./gen_learns -i 4 -v $VAL4 > /dev/null
sleep 1

echo "Sending learns for 5"
./gen_learns -i 5 -v $VAL5 > /dev/null
sleep 1

echo "Sending learns for 3"
./gen_learns -i 3 -v $VAL3 > /dev/null
sleep 1

echo "Sending learns for 6"
./gen_learns -i 2 -v $VAL2 > /dev/null

sleep 5

# Kill Logger
kill -s INT $NET_PID

# Kill all test and verify that they were already completed
KILL_FILE=lt2.kill
killall learner_main &> $KILL_FILE
K_COUNT=`cat $KILL_FILE |grep "No matching" | wc -l`
rm -rf $KILL_FILE
let K_COUNT=K_COUNT+0

if [[ $K_COUNT -lt 1 ]]; then
	
	echo "Learner did not terminate!"
	exit $FAILURE
fi


cat $OUT_LEA | grep "LEARNED_VAL:" | awk '{print $3}' > $OUT_DIFF
DIFF=`diff $OUT_DIFF $OUT_CMP | wc -l`
let DIFF=DIFF+0

# Check results

if [[ $DIFF -gt 0 ]]; then
	echo "Learned value do not match!!"
	exit $FAILURE
fi

echo "Test successful"
exit $SUCCESS


# Return $SUCCESS or $FAILURE
# 
# if [[ $EXITVAL == 1 ]]; then
# 	echo "X some error!"
# 	exit $FAILURE
# fi
# 
# if [[ $EXITVAL == 2 ]]; then
# 	echo "X some error!"
# 	exit $FAILURE
# fi
# 
# if [[ $EXITVAL == 3 ]]; then
# 	echo "X some error!"
# 	exit $FAILURE
# fi
# 
# echo "Unknow exit status: $EXITVAL"
# exit $FAILURE
# 
# 
