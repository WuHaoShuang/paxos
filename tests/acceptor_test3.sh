#!/bin/bash

# Check dir
DIR=`pwd`;
if [ `basename $DIR` != "tests" ]; then
    echo "Please run this script from tests/"
	exit
fi

ARR_SIZE=`grep "define ACCEPTOR_ARRAY_SIZE" ../config.h | awk '{print $3}'`
let ARR_SIZE=ARR_SIZE+0;
echo "libpaxos is configured for acceptor_array_size = $ARR_SIZE"
if [ $ARR_SIZE -lt 1 ]; then
    echo "Failed to parse acceptor array size: $ARR_SIZE"
	exit
fi

ANSWERS="$DIR/ac3_correct.log"

# Parametrize output files
OUT_ACC="$DIR/ac3_log.txt"
OUT_NET="$DIR/ac3_net.txt"
OUT_DIF="$DIR/ac3_diff.txt"


# Clear previous test results
rm -rf $OUT_ACC $OUT_NET $OUT_DIF

# Start and detach process, save pid
echo "Starting network logger"
./packet_logger -f $OUT_NET > /dev/null &
NET_PID=$!;

# Start and detach process, save pid
echo "Starting network acceptor"
./acceptor_main > $OUT_ACC &
ACC_PID=$!;

sleep 2

echo "Send valid prepare for instance 0"
./gen_prep -f 0 -n 1 -b 201 > /dev/null
sleep 1

echo "Send valid prepare for iid that should overwrite"
./gen_prep -f $ARR_SIZE -n 1 -b 101 > /dev/null
sleep 1


echo "Send valid accept for instance 0"
./gen_accept -i 0 -b 201 -v "test_value_to_archive" > /dev/null
sleep 1

echo "Send valid accept for instance Acceptor_array_size"
./gen_accept -i $ARR_SIZE -b 101 -v "test_value_that_overwrites" > /dev/null
sleep 1

echo "Require a learner sync for both instances"
./gen_lsync -n 1 -f 0  > /dev/null
./gen_lsync -n 1 -f $ARR_SIZE  > /dev/null

# Wait for test termination
sleep 3
# Kill Processes
kill -s INT $ACC_PID $NET_PID

# Check results

diff $OUT_NET $ANSWERS > $OUT_DIF
D_LINES=`cat $OUT_DIF | wc -l`; let D_LINES=D_LINES+0
if [[ $D_LINES == 0 ]]; then
	echo "No differences"
else
	echo "There are differences in output!"
	echo "Check diff file $OUT_DIF"
	exit $FAILURE	
fi

# Return $SUCCESS or $FAILURE
exit $SUCCESS
