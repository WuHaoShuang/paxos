#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>

int main (int argc, char const *argv[]) {
    int                         s;
	socklen_t                   addrlen;
	struct                      sockaddr_in addr;
    
    /* Set up socket */
    s = socket(AF_INET, SOCK_DGRAM, 0);
    if (s < 0) {
        perror("sender socket");
        exit(0);
    }
    
    /* Set up address */
    addr.sin_addr.s_addr = inet_addr("239.0.0.1");
    if (addr.sin_addr.s_addr == INADDR_NONE) {
        printf("Error setting sender->addr\n");
        exit(0);
    }
    
    addr.sin_family = AF_INET;
    addr.sin_port = htons((uint16_t)8888);	
    addrlen = sizeof(struct sockaddr_in);

    int optval;
    socklen_t optlen = 4;
    getsockopt(s, SOL_SOCKET, SO_SNDBUF, (int *)&optval, &optlen);
    
    printf("%d\n", optval);
    printf("%d\n", optlen);
    return 0;
}
