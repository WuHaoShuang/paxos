#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include "libpaxos.h"

static int proposer_id = 0;
static int wait_interval = 1000; //usec
static int max_val_size = PAXOS_MAX_VALUE_SIZE;
static int min_val_size = 4;
static int random_seed = 1234;
static int max_queue_size = 10;

static char val_buffer[PAXOS_MAX_VALUE_SIZE];
static deliver_function deliver_callback = NULL;

int deliver_count = 0;
void example_callback(char* val, int size) {
    printf("Del %d: size:%d starts with %c\n", deliver_count, size, val[0]);
    deliver_count++;
}

void handle_cltr_c (int sig) {
    proposer_print_event_counters();
    exit (0);
}

void pusage() {
    printf("This proposer sends pseudo random values at infinitum!\n");
    
    printf("-i N    : starts proposer with id N (default is 0)\n");
    printf("-w N    : waits N microseconds when idle\n");
    printf("-m N    : min value size is N (default: 4)\n");
    printf("-M N    : max value size is N (default: %d)\n", PAXOS_MAX_VALUE_SIZE);
    printf("-s N    : seed for random packet generation (default: 1234)\n");
    printf("-q N    : if the proposer queue size exceedes N, value submission idles a while\n");
    printf("-d      : Activates proposer delivery callback\n");

    printf("-h   : prints this message and exits\n");
}

void parse_args(int argc, char * const argv[]) {
    int c, n;
    while((c = getopt(argc, argv, "i:w:m:M:s:q:dh")) != -1) {
        switch(c) {
            case 'i': {
                proposer_id = atoi(optarg);
            }
            break;

            case 'w': {
                wait_interval = atoi(optarg);
            }
            break;

            case 'm': {
                n = atoi(optarg);
                if(n > 4 && n < PAXOS_MAX_VALUE_SIZE) {
                    min_val_size = n;
                }
            }
            break;
            
            case 'M': {
                n = atoi(optarg);
                if(n > 0 && n < PAXOS_MAX_VALUE_SIZE) {
                    max_val_size = n;
                }
            }
            break;

            case 's': {
                random_seed = atoi(optarg);
            }
            break;
            
            case 'q': {
                max_queue_size = atoi(optarg);
            }
            break;

            case 'd': {
                deliver_callback = example_callback;
            }
            break;
            
            case 'h':
            default: {
                pusage();
                exit(0);
            }
        }
    }
    
    srandom(random_seed);
}


#define PRINT_STEP 5000

static long last_val_size = -1;
int generate_random_value(char * buf) {
    long r = random();
    int val_size = (r % ((max_val_size - min_val_size)+1)) + min_val_size;
    while(val_size == last_val_size) {
        //For sanity check should not propose 
        //twice the same value in a row!
        r = random();
        val_size = (r % ((max_val_size - min_val_size)+1)) + min_val_size;
    }
    last_val_size = val_size;
    unsigned char c = (r % 92) + 33;
    memset(buf, c, val_size);
    return val_size;
}

void send_untildeath_parameters() {
    
    int params[3];
    params[0] = random_seed;
    params[1] = min_val_size;
    params[2] = max_val_size;
    
    printf("Submit values size %d to %d\n", min_val_size, max_val_size);
    printf("Max queue size:%d, idles for %dusec\n", max_queue_size, wait_interval);
    
    proposer_submit_value((char*)&params, (sizeof(int)*3));
    
}

int main (int argc, char const *argv[]) {    
    signal (SIGINT, handle_cltr_c);
    
    parse_args(argc, (char **)argv);

    printf("Proposer_id is:%d\n", proposer_id);
    
    if (proposer_init_and_deliver(proposer_id, deliver_callback) == -1) {
        printf("Failed to intialize proposer %d!\n", proposer_id);
        exit(1);
    }

    send_untildeath_parameters();
    
    int count = 0;    
    int qsize, val_size;
    // size_t i;
    sleep(2);
    
    while(1) {
        qsize = proposer_queue_size();
        
        if(qsize < max_queue_size) {            
            val_size = generate_random_value(val_buffer);
            proposer_submit_value(val_buffer, val_size);
            count++;

            if ((count % PRINT_STEP) == 0) {
                printf("%d values submitted\n", count);
            }

        } else {
            // printf(".");
            usleep(wait_interval);
        }
    }
    return 0;
}

