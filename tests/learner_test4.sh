#!/bin/bash

# Check dir
DIR=`pwd`;
if [ `basename $DIR` != "tests" ]; then
    echo "Please run this script from tests/"
	exit
fi

LEA_ARRAY_SIZE=`grep "define LEARNER_ARRAY_SIZE" ../config.h | awk '{print $3}'`
echo "libpaxos is configured for learning window of $LEA_ARRAY_SIZE"
let LEA_ARRAY_SIZE=LEA_ARRAY_SIZE-1;

# Parametrize output files
OUT_NET="$DIR/lt_log4.txt"
OUT_LEA="$DIR/lt_lea4.txt"
OUT_CMP="$DIR/lt_cmp4.txt"
OUT_DIFF="$DIR/lt_diff4.txt"

# Clear previous test results
rm -rf $OUT_NET $OUT_LEA $OUT_CMP $OUT_DIFF

# Start and detach process, save pid
echo "Starting network logger"
./packet_logger -f $OUT_NET > /dev/null &
NET_PID=$!;


# Start and detach process, save pid
echo "Starting learner"
let KILL_AFTER=LEA_ARRAY_SIZE+1;
./learner_main -k $KILL_AFTER > $OUT_LEA &
# LEA_PID=$!;

sleep 3

# Skip 0
COUNTER=1
echo "Sending learns from $COUNTER to $LEA_ARRAY_SIZE"
while [  $COUNTER -lt $LEA_ARRAY_SIZE ]; do
  ./gen_learns -i $COUNTER -v "$COUNTER" > /dev/null  
  let COUNTER=COUNTER+1 
done

# Learner should ignore that
echo "Sending learns for instance $COUNTER << BAD"
./gen_learns -i $COUNTER -v "BAD BAD VALUE!!!" > /dev/null
sleep 1

# Close 0
echo "Sending learns for instance 0"
./gen_learns -i 0 -v "0" > /dev/null

# Learner should now close last instance
echo "Sending learns for instance $COUNTER"
./gen_learns -i $COUNTER -v "$COUNTER" > /dev/null


sleep 3

# Kill Logger
kill -s INT $NET_PID

# Kill all test and verify that they were already completed
KILL_FILE=lt4.kill
killall learner_main &> $KILL_FILE
K_COUNT=`cat $KILL_FILE |grep "No matching" | wc -l`
rm -rf $KILL_FILE
let K_COUNT=K_COUNT+0

if [[ $K_COUNT -lt 1 ]]; then
	echo "Learner did not terminate!"
	exit $FAILURE
fi

# Filter out all learned values
cat $OUT_LEA | grep "LEARNED_VAL:" | awk '{print $3}' > $OUT_DIFF

# Check values learned
COUNTER=0;
for line in $(cat $OUT_DIFF); do	
	if [[ "$line" != "$COUNTER" ]]; then
		echo "Learned value do not match!!"
		echo "Expected: $COUNTER Got: $line"
		exit $FAILURE
	fi
	let COUNTER=COUNTER+1 
done

#Check number of learned values
let LEA_ARRAY_SIZE=LEA_ARRAY_SIZE+1
if [[ $LEA_ARRAY_SIZE != $COUNTER ]]; then
	echo "Error: $COUNTER values learned (instead of $LEA_ARRAY_SIZE)"
	exit $FAILURE
fi

echo "Test successful"
exit $SUCCESS


# Return $SUCCESS or $FAILURE
# 
# if [[ $EXITVAL == 1 ]]; then
# 	echo "X some error!"
# 	exit $FAILURE
# fi
# 
# if [[ $EXITVAL == 2 ]]; then
# 	echo "X some error!"
# 	exit $FAILURE
# fi
# 
# if [[ $EXITVAL == 3 ]]; then
# 	echo "X some error!"
# 	exit $FAILURE
# fi
# 
# echo "Unknow exit status: $EXITVAL"
# exit $FAILURE
# 
# 
