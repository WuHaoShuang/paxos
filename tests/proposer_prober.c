#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>

#include "libpaxos.h"

static int proposer_id = 0;
static int val_per_sec = 10;
static int max_val_size = PAXOS_MAX_VALUE_SIZE;
static int min_val_size = 4;
static int random_seed = -1;

static time_t start_time;
static char val_buffer[PAXOS_MAX_VALUE_SIZE];
static int count = 0;
static int probe_id = 0;

//In sec
#define PRINT_INTERVAL 5
//Sleep in main loop, should be probe interval/3 or so
#define CYCLE_TIME_USEC 500000
//How frequently probes are sent
#define PROBE_INTERVAL_USEC 2000000


void handle_cltr_c (int sig) {
    int seconds = time(NULL) - start_time;
    printf("Submitted %d values in %d seconds (%f vps) + %d probes\n", count, seconds, ((float)count/seconds), (probe_id));
    proposer_print_event_counters();
    
    exit (0);
}

void pusage() {
    printf("-i N    : starts proposer with id N (default is 0)\n");
    printf("-v N    : proposes N values per seconds (probes excluded)\n");
    printf("-m N    : min value size is N (default: 4)\n");
    printf("-M N    : max value size is N (default: %d)\n", PAXOS_MAX_VALUE_SIZE);
    printf("-s N    : seed for random packet generation (default: time())\n");

    printf("-h   : prints this message and exits\n");
}

void parse_args(int argc, char * const argv[]) {
    int c, n;
    while((c = getopt(argc, argv, "i:v:m:M:s:h")) != -1) {
        switch(c) {
            case 'i': {
                proposer_id = atoi(optarg);
            }
            break;

            case 'v': {
                val_per_sec = atoi(optarg);
            }
            break;

            case 'm': {
                n = atoi(optarg);
                if(n > 4 && n < PAXOS_MAX_VALUE_SIZE) {
                    min_val_size = n;
                }
            }
            break;
            
            case 'M': {
                n = atoi(optarg);
                if(n > 0 && n < PAXOS_MAX_VALUE_SIZE) {
                    max_val_size = n;
                }
            }
            break;

            case 's': {
                random_seed = atoi(optarg);
            }
            break;
                        
            case 'h':
            default: {
                pusage();
                exit(0);
            }
        }
    }
    
    if(random_seed == -1){
        random_seed = time(NULL);
    }
    srandom(random_seed);
    
}


//To avoid excessive locking, 
//after asking for queue size, STEP values are submitted

int generate_random_value(char * buf) {
    long r = random();
    int val_size = (r % ((max_val_size - min_val_size)+1)) + min_val_size;
    int passes = val_size / sizeof(long);
    long * ary = (long*) buf;
    int i;
    for(i = 0; i < passes; i++) {
        ary[i] = r;
    }
    return i*sizeof(long);
}

int submit_n_values(int to_submit) {
    int qsize = proposer_queue_size();
    int i, val_size;
    lock_proposer_submit();
    for(i = 0; i < to_submit; i++) {
        val_size = generate_random_value(val_buffer);
        nolock_proposer_submit_value(val_buffer, val_size);
    }
    unlock_proposer_submit();
    return qsize;
}

struct probemsg {
    unsigned int header;
    struct timeval timestamp;
    int id;
    unsigned int hash;
};
#define PROBE_HEADER 0xDEADBEE
unsigned int djb2_hash(char *str, int size) {
    unsigned long hash = 5381;
    int i;

    for (i = 0; i < size; i++) {
        hash = ((hash << 5) + hash) + str[i]; /* hash * 33 + c */
    }

    return hash;
}
void submit_probe() {
    struct probemsg * p = (struct probemsg *)&val_buffer;
    p->header = PROBE_HEADER;
    gettimeofday(&p->timestamp, NULL);
    p->id = probe_id++;
    
    //calc hash
    p->hash = 0;
    p->hash = djb2_hash(val_buffer, sizeof(struct probemsg));
    
    proposer_submit_value(val_buffer, sizeof(struct probemsg));
}

int main (int argc, char const *argv[]) {    
    signal (SIGINT, handle_cltr_c);
    
    parse_args(argc, (char **)argv);

    printf("Proposer_id is:%d\n", proposer_id);
    
    if (proposer_init(proposer_id) == -1) {
        printf("Failed to intialize proposer %d!\n", proposer_id);
        exit(1);
    }
    printf("Submit at %d values per second (size %d to %d)\n", val_per_sec, min_val_size, max_val_size);
    sleep(2);

    start_time = time(NULL);
    int queue_size = 0;
       
    struct timeval submit_time;
    struct timeval probe_time;
    struct timeval print_time;
    gettimeofday(&submit_time, NULL);
    probe_time = submit_time;
    print_time = submit_time;
    
    struct timeval now_time;
    while(1) {
        usleep(CYCLE_TIME_USEC);
        gettimeofday(&now_time, NULL);
        
        //Check if it's time to submit new values
        if(now_time.tv_sec > submit_time.tv_sec) {
            queue_size = submit_n_values(val_per_sec);
            submit_time.tv_sec = now_time.tv_sec;
            count += val_per_sec;
            
            //Check if it's time to print count
            if((now_time.tv_sec - print_time.tv_sec) > PRINT_INTERVAL) {
                print_time.tv_sec = now_time.tv_sec;
                printf("%d submitted, prop q.size: %d\n",count, queue_size);
            }
        }

        //Check if it's time to submit new probe
        long unsigned int time_diff;
        time_diff = (now_time.tv_sec - probe_time.tv_sec) * 1000000;
        time_diff += (now_time.tv_usec - probe_time.tv_usec);
        if(time_diff > PROBE_INTERVAL_USEC) {
            submit_probe();
            probe_time = now_time;
        }
        
    }
    return 0;
}

