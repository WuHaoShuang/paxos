#!/bin/bash

# Check dir
DIR=`pwd`;
if [ `basename $DIR` != "tests" ]; then
    echo "Please run this script from tests/"
	exit
fi

N_OF_ACCEPTORS=`grep "define N_OF_ACCEPTORS" ../config.h | awk '{print $3}'`
let N_OF_ACCEPTORS=N_OF_ACCEPTORS+0;
echo "libpaxos is configured for $N_OF_ACCEPTORS acceptors"
if [ $N_OF_ACCEPTORS -lt 3 ]; then
    echo "We need at least 3 acceptors for this test"
	exit
fi
let HALF=(N_OF_ACCEPTORS/2)
let LAST_ACC_ID=(N_OF_ACCEPTORS - 1)

# Parametrize output files
OUT_NET="$DIR/lt_log2.txt"
OUT_LEA="$DIR/lt_lea2.txt"

# Clear previous test results
rm -rf $OUT_NET $OUT_LEA

# Start and detach process, save pid
echo "Starting network logger"
./packet_logger -f $OUT_NET > /dev/null &
NET_PID=$!;

# Start and detach process, save pid
echo "Starting learner"
./learner_main -k 1 > $OUT_LEA &
# LEA_PID=$!;

echo "Sending first round of accepts"
./gen_learns -v "1asdfasdasdfff" -a $HALF -b 101 > /dev/null
sleep 1

echo "Sending second round of accepts"
./gen_learns -v "2asdfasdasdfff" -a 1 -f $LAST_ACC_ID -b 102 > /dev/null
sleep 1

LAST_VALUE="3asdfasdasdfff"
echo "Sending third round of accepts"
./gen_learns -v $LAST_VALUE -a $HALF -b 201 > /dev/null
sleep 1

echo "Sending last accept"
./gen_learns -v $LAST_VALUE -a 1 -f $LAST_ACC_ID -b 201 > /dev/null

sleep 3


# Kill Logger
kill -s INT $NET_PID

# Kill all test and verify that they were already completed
KILL_FILE=lt2.kill
killall learner_main &> $KILL_FILE
K_COUNT=`cat $KILL_FILE |grep "No matching" | wc -l`
rm -rf $KILL_FILE
let K_COUNT=K_COUNT+0

if [[ $K_COUNT -lt 1 ]]; then
	
	echo "Learner did not terminate!"
	exit $FAILURE
fi

# Check results
L_LINES=`cat $OUT_LEA | grep $LAST_VALUE |wc -l` 

if [[ $L_LINES -lt 1 ]]; then
	echo "Valuenot found in $OUT_LEA"
	exit $FAILURE
fi

echo "Test successful"
exit $SUCCESS


# Return $SUCCESS or $FAILURE
# 
# if [[ $EXITVAL == 1 ]]; then
# 	echo "X some error!"
# 	exit $FAILURE
# fi
# 
# if [[ $EXITVAL == 2 ]]; then
# 	echo "X some error!"
# 	exit $FAILURE
# fi
# 
# if [[ $EXITVAL == 3 ]]; then
# 	echo "X some error!"
# 	exit $FAILURE
# fi
# 
# echo "Unknow exit status: $EXITVAL"
# exit $FAILURE
# 
# 
