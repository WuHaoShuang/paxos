#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>

#include "libpaxos.h"


static int learn_count = 0;
static int probe_count = 0;
static int mute = 0;
static char* filename = NULL;
static FILE * probelog = NULL;
                
void pusage() {
    printf("-f F : write probe log also to file F\n");
    printf("-m   : don't write probe to stdout\n");
    printf("-h   : prints this message\n");
}


void handle_cltr_c (int sig) {
    if (filename != NULL) {
        fclose(probelog);
    }
    printf("Total %d val + %d probes\n", learn_count, probe_count);
    learner_print_event_counters();
    exit (0);
}

void parse_args(int argc, char * const argv[]) {
    int c;
    while((c = getopt(argc, argv, "f:mh")) != -1) {
        switch(c) {
            case 'f': {
                filename = optarg;
                probelog = fopen(filename, "w");
                if (probelog == NULL) {
                    printf("Cannot log probes to %s\n", filename);
                    exit(0);
                }
            }
            break;

            case 'm': {
                mute = 1;
            }
            break;
            
            case 'h':
            default: {
                pusage();
                exit(0);
            }
        }
    }

}

struct probemsg {
    unsigned int header;
    struct timeval timestamp;
    int id;
    unsigned int hash;
};
#define PROBE_HEADER 0xDEADBEE
unsigned int djb2_hash(char *str, int size) {
    unsigned long hash = 5381;
    int i;

    for (i = 0; i < size; i++) {
        hash = ((hash << 5) + hash) + str[i]; /* hash * 33 + c */
    }

    return hash;
}

int is_probe(char* buf, int size) {
    struct probemsg * p;
    if (size != sizeof(struct probemsg))
        return 0;
    p = (struct probemsg*) buf;
    if (p->header != PROBE_HEADER)
        return 0;
    unsigned int tmphash = p->hash;
    p->hash = 0;
    unsigned int calchash = djb2_hash(buf, sizeof(struct probemsg));

    if(tmphash != calchash) {
        return 0;
    }
        
    return 1;
}
void handle_probe(struct probemsg* p) {
    struct timeval time_now;
    struct timeval * tstamp;
    
    long unsigned int timediff = 0;
    gettimeofday(&time_now, NULL);
    tstamp = &p->timestamp;

    timediff += (time_now.tv_sec - tstamp->tv_sec) * 1000000;
    timediff += (time_now.tv_usec - tstamp->tv_usec);
    
    if(probelog != NULL) {
        fprintf(probelog, "%d,%lu\n", p->id, timediff);
    }
    
    if(mute == 0) {
        fprintf(stdout, "%d,%lu\n", p->id, timediff);
    }
}

int main (int argc, char const *argv[]) {
    signal (SIGINT, handle_cltr_c);
    
    parse_args(argc, (char **)argv);

    if (learner_init() == -1) {
        printf("Learner init failed.\n");
        exit(1);
    }
    
    char * v;
    int val_size;
    while(1) {
        val_size = learner_get_next_value(&v);
        if(is_probe(v, val_size)) {
            handle_probe((struct probemsg*)v);
            probe_count++;
        } else {
            learn_count++;
        }
        free(v);
    }
    return 0;
}

