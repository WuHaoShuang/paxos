#!/bin/bash

# Check dir
DIR=`pwd`;
if [ `basename $DIR` != "tests" ]; then
    echo "Please run this script from tests/"
	exit
fi

# Parametrize output files
OUT_NET="$DIR/lt_log1.txt"
OUT_LEA="$DIR/lt_lea1.txt"

# Clear previous test results
rm -rf $OUT_NET $OUT_LEA

# Start and detach process, save pid
echo "Starting network logger"
./packet_logger -f $OUT_NET > /dev/null &
NET_PID=$!;

# Start and detach process, save pid
echo "Starting network learner"
./learner_main -k 3 > $OUT_LEA &
# LEA_PID=$!;

echo "Sending accept for 1"
./gen_learns -i 1 -v "asdfasdasdfff" > /dev/null
sleep 1
echo "Sending accept for 2"
./gen_learns -i 2 -v "cdspjewp394n4n3n3-" > /dev/null
sleep 1
echo "Sending accept for 0"
./gen_learns -i 0 -v "nspioj3-9023pe23-9jc23in2pe3e2-3f23d" > /dev/null
sleep 5

# Kill Logger
kill -s INT $NET_PID

# Kill all test and verify that they were already completed
KILL_FILE=lt1.kill
killall learner_main &> $KILL_FILE
K_COUNT=`cat $KILL_FILE |grep "No matching" | wc -l`
rm -rf $KILL_FILE
let K_COUNT=K_COUNT+0

if [[ $K_COUNT -lt 1 ]]; then
	
	echo "Learner did not terminate!"
	exit $FAILURE
fi

echo "Test successful"
exit $SUCCESS

# # Check results
# X_LINES=`cat $OUT_X | wc -l` 

# Return $SUCCESS or $FAILURE
# if [[ $EXITVAL == 0 ]]; then
# 	exit $SUCCESS
# fi
# 
# if [[ $EXITVAL == 1 ]]; then
# 	echo "X some error!"
# 	exit $FAILURE
# fi
# 
# if [[ $EXITVAL == 2 ]]; then
# 	echo "X some error!"
# 	exit $FAILURE
# fi
# 
# if [[ $EXITVAL == 3 ]]; then
# 	echo "X some error!"
# 	exit $FAILURE
# fi
# 
# echo "Unknow exit status: $EXITVAL"
# exit $FAILURE
# 
# 
