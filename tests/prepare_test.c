#include <stdlib.h>
#include <unistd.h>
#include "multicast.h"
#include "libpaxos_priv.h"

int main (int argc, char const *argv[]) {
    int i = 0;
    int prep_msgs = 10;
    multicast_msg* mm;
    
    int batch_size = sizeof(prepare_batch_msg) + (sizeof(prepare_msg) * prep_msgs);
    prepare_batch_msg* bmsg = malloc(batch_size);
    
    prepare_msg* pmsg;
    multicast_sender* as = multicast_sender_new(PAXOS_ACCEPTORS_NET);
    multicast_receiver* pr = multicast_receiver_new(PAXOS_PROPOSERS_NET);
    
    bmsg->count = 10;
    
    for(i = 0; i < prep_msgs; i++) {
        pmsg = (prepare_msg*) &bmsg->data[i*sizeof(prepare_msg)];
        pmsg->iid = i;
        pmsg->ballot = 101;
    }
    
    
    multicast_send(as, (char*) bmsg, batch_size, PAXOS_PREPARE);
    
    int promise_count = 0;
    
    promise_batch_msg* prbmsg;
    while(1) {
        if ((mm = multicast_recv(pr)) != NULL) {
            prbmsg = (promise_batch_msg*) mm->data;
            promise_count += prbmsg->count;
            
            if (promise_count == 10) {
                break;
            }
        } else {
            sleep(1);
        }
    }
    
    return 0;
}

