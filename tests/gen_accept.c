#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "multicast.h"
#include "libpaxos_priv.h"

void pusage() {
    printf("-i N : sends accepts for instance N\n");
    printf("-b N : sends accepts with ballot N\n");
    
    printf("-r N : generates a random value of size N\n");
    printf("-v val : sends val as value in the accepts\n");

    printf("-h   : prints this message and exits\n");
}

int instance_id = 0;
int ballot = 101;
char* value = "0123456789";
int value_size = 11;


void gen_random_val() {
    int i;
    
    value =  malloc(value_size);
    
    for(i = 0; i < value_size; i++) {
        value[i] = (char) ((rand() % 25) + 65);
    }
    value[i-1] = '\0'; 
}

void parse_args(int argc, char * const argv[]) {
    int c;
    while((c = getopt(argc, argv, "i:b:r:v:h")) != -1) {
        switch(c) {            
            case 'i': {
                instance_id = atoi(optarg);
            }
            break;
            
            case 'b': {
                ballot = atoi(optarg);
            }
            break;
            
            case 'r': {
                value_size = atoi(optarg);
                gen_random_val();
            }
            break;
            
            case 'v': {
                value_size = strlen(optarg)+1;
                value = optarg;
            }
            break;
            
            case 'h':
            default: {
                pusage();
                exit(0);
            }
        }
    }    
}

char send_buffer[MC_MSG_MAX_DATA_SIZE];


void gen_and_send() {
    
    multicast_sender* s = multicast_sender_new(PAXOS_ACCEPTORS_NET);
    accept_msg * amsg;
    
    if (s == NULL) {
        printf("Failed to initialize sender. Exit.\n");
        exit(0);
    }

    int sres;
    amsg = (accept_msg *)send_buffer;
    amsg->iid = instance_id;
    amsg->ballot = ballot;
    amsg->value_size = value_size;
    memcpy(amsg->value, value, amsg->value_size);
    
    sres = multicast_send(s, send_buffer, (sizeof(accept_msg) + amsg->value_size), PAXOS_ACCEPT);
    if(sres == -1) {
        printf("Multicast send error. Exit\n");
        exit(0);
    }
}

int main (int argc, char const *argv[]) {
    
    srand(time(NULL));

    parse_args(argc, (char **)argv);
    
    printf("Will send an accept for iid:%d, ballot:%d, val:%s [%d bytes]\n", instance_id, ballot, value, value_size);
    
    gen_and_send();
    return 0;
}

