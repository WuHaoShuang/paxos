#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <string.h>
#include <time.h>

#include "libpaxos.h"

static int proposer_id = 0;
static int wait_interval = 10000; //usec
static int val_count = 1000;
static int max_val_size = PAXOS_MAX_VALUE_SIZE;
static int min_val_size = 4;
static int random_seed = -1;
static int max_queue_size = 100;

static char val_buffer[PAXOS_MAX_VALUE_SIZE];


void handle_cltr_c (int sig) {
    proposer_print_event_counters();
    exit (0);
}

void pusage() {
    printf("-i N    : starts proposer with id N (default is 0)\n");
    printf("-v N    : proposes N values\n");    
    printf("-w N    : waits N microseconds when idle\n");
    printf("-m N    : min value size is N (default: 4)\n");
    printf("-M N    : max value size is N (default: %d)\n", PAXOS_MAX_VALUE_SIZE);
    printf("-s N    : seed for random packet generation (default: time())\n");
    printf("-q N    : if the proposer queue size exceedes N, value submission idles a while\n");

    printf("-h   : prints this message and exits\n");
}

void parse_args(int argc, char * const argv[]) {
    int c, n;
    while((c = getopt(argc, argv, "i:v:w:m:M:s:q:h")) != -1) {
        switch(c) {
            case 'i': {
                proposer_id = atoi(optarg);
            }
            break;

            case 'v': {
                val_count = atoi(optarg);
            }
            break;

            case 'w': {
                wait_interval = atoi(optarg);
            }
            break;

            case 'm': {
                n = atoi(optarg);
                if(n > 4 && n < PAXOS_MAX_VALUE_SIZE) {
                    min_val_size = n;
                }
            }
            break;
            
            case 'M': {
                n = atoi(optarg);
                if(n > 0 && n < PAXOS_MAX_VALUE_SIZE) {
                    max_val_size = n;
                }
            }
            break;

            case 's': {
                random_seed = atoi(optarg);
            }
            break;
            
            case 'q': {
                max_queue_size = atoi(optarg);
            }
            break;
            
            case 'h':
            default: {
                pusage();
                exit(0);
            }
        }
    }
    
    if(random_seed == -1){
        random_seed = time(NULL);
    }
    srandom(random_seed);
    
}


//To avoid excessive locking, 
//after asking for queue size, STEP values are submitted
#define STEP 100
#define PRINT_STEP 5000

int generate_random_value(char * buf) {
    long r = random();
    int val_size = (r % ((max_val_size - min_val_size)+1)) + min_val_size;
    int passes = val_size / sizeof(long);
    long * ary = (long*) buf;
    int i;
    for(i = 0; i < passes; i++) {
        ary[i] = r;
    }
    return i*sizeof(long);
}

int main (int argc, char const *argv[]) {    
    signal (SIGINT, handle_cltr_c);
    
    parse_args(argc, (char **)argv);

    printf("Proposer_id is:%d\n", proposer_id);
    
    if (proposer_init(proposer_id) == -1) {
        printf("Failed to intialize proposer %d!\n", proposer_id);
        exit(1);
    }

    printf("Submit at least %d values (size %d to %d)\n", val_count, min_val_size, max_val_size);
    printf("Max queue size:%d, Step:%d, idles for %dusec\n", max_queue_size, STEP, wait_interval);
    
    int count = 0;    
    int qsize, val_size;
    size_t i;
    sleep(2);
    time_t start_time = time(NULL);
    
    while(count < val_count) {
        qsize = proposer_queue_size();
        if(qsize < max_queue_size) {
            
            lock_proposer_submit();
            for(i = 0; i < STEP; ++i) {
                val_size = generate_random_value(val_buffer);
                // proposer_submit_value(val_buffer, val_size);
                nolock_proposer_submit_value(val_buffer, val_size);
            }
            unlock_proposer_submit();
            count += STEP;
            if ((count % PRINT_STEP) == 0) {
                printf("%d values submitted (%d left)\n", count, (val_count - count));
            }
        } else {
            // printf(".");
            usleep(wait_interval);
        }
    }
    printf("\nAll values submitted (%d).\n", count);
    while(proposer_queue_size() > 0) {
        usleep(wait_interval);        
    }

    time_t end_time = time(NULL);
    time_t seconds = end_time - start_time;
    float rate = ((float)count)/seconds;
    printf("%d values in %d seconds -> %f VPS!\n",count, (int)seconds, rate);

    proposer_print_event_counters();
    return 0;
}

