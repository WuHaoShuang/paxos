#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include "memory.h"

#include "libpaxos.h"

static int exit_after = 100;
static int print_multiple = 1;
static int mute = 0;
static char * values_file_path = NULL;
FILE * values_file = NULL;

void terminate() {
    system("killall -INT untildeath_proposer");
    if(values_file != NULL) {
        fclose(values_file);
    }
    exit(1);    
}

void pusage() {
    printf("This learner waits for values from an untildeath_proposer\n");
    printf("The same pseudo-random sequence is produced and checked\n");
    
    printf("-p N : Prints updates every N messages (default:1 = print all)\n");
    printf("-m   : Mute (overrides -p)\n");
    printf("-o F : Writes values delivered to file F\n");
    printf("-h   : prints this message\n");
}

void parse_args(int argc, char * const argv[]) {
    int c;
    while((c = getopt(argc, argv, "k:p:mo:")) != -1) {
        switch(c) {
            case 'k': {
                exit_after = atoi(optarg);
            }
            break;

            case 'p': {
                print_multiple = atoi(optarg);
            }
            break;

            case 'm': {
                mute = 1;
            }
            break;

            case 'o': {
                values_file_path = optarg;
            }
            break;
            
            case 'h':
            default: {
                pusage();
                exit(0);
            }
        }
    }
    
    if (values_file_path != NULL) {
        values_file = fopen(values_file_path, "w");
        if(values_file == NULL) {
            printf("Could not open file:%s\n", values_file_path);
            perror("open");
            terminate();
        }        
    }
}

static char val_buffer[PAXOS_MAX_VALUE_SIZE];
static int max_val_size = -1;
static int min_val_size = -1;
static int random_seed = -1;

static long last_val_size = -1;
int generate_random_value(char * buf) {
    long r = random();
    int val_size = (r % ((max_val_size - min_val_size)+1)) + min_val_size;
    while(val_size == last_val_size) {
        //For sanity check should not propose 
        //twice the same value in a row!
        r = random();
        val_size = (r % ((max_val_size - min_val_size)+1)) + min_val_size;
    }
    last_val_size = val_size;
    unsigned char c = (r % 92) + 33;
    memset(buf, c, val_size);
    return val_size;
}


void wait_for_parameters () {
    char * v;
    int val_size;

    //First value delivered should contain random seed info!
    val_size = learner_get_next_value(&v);
    if (val_size != (sizeof(int)*3)) {
        printf("First value is not a valid random seed info! (size %d)\n", val_size);
        exit(1);
    }
    random_seed = ((int*)v)[0];
    min_val_size = ((int*)v)[1];
    max_val_size = ((int*)v)[2];
    
    if(min_val_size < 1 || max_val_size > PAXOS_MAX_VALUE_SIZE) {
        printf("invalid size parameters! %d...%d\n", min_val_size, max_val_size);
        terminate();
    }
    
    printf("Learner received parameters, seed:%d values range[%d..%d]\n", random_seed, min_val_size, max_val_size);
    
}

void print_value(int curr_iid, char * buf, int size) {
    if(values_file == NULL)
        return;
        
    fprintf(values_file, 
        "%d:(size:%d)[%c][%c][%c][...]\n",
        curr_iid, size, 
        buf[0], buf[1], buf[2]);
}

int main (int argc, char const *argv[]) {
    signal (SIGINT, terminate);
    parse_args(argc, (char **)argv);

    if (learner_init() == -1) {
        printf("Learner init failed.\n");
        terminate();
    }
    
    wait_for_parameters();

    int current_instance_id = 0;
    int expected_size, val_size;
    char * v;
    srandom(random_seed);

    while(1) {
        current_instance_id++;
        expected_size = generate_random_value(val_buffer);        
        print_value(current_instance_id, val_buffer, expected_size);
        val_size = learner_get_next_value(&v);
        
        if((expected_size == val_size) &&
            (memcmp(&val_buffer, v, val_size) == 0)) {
            free(v);

            if(!mute && ((current_instance_id % print_multiple) == 0))
                printf("LEARNED_VAL: %d (size:%d) \n", current_instance_id, val_size);

        } else {
            printf("Error at instance %d expected size:%d received:%d\n", current_instance_id, expected_size, val_size);
            printf("expected: [%c][%c][%c][...] (%d)\n", val_buffer[0], val_buffer[1], val_buffer[2], expected_size);
            printf("received: [%c][%c][%c][...] (%d)\n", v[0], v[1], v[2], val_size);
            terminate();
        }
    }

    return 0;
}

