#!/bin/bash
TEST="	hello_worlds.sh \
		learner_test1.sh \
		learner_test2.sh \
		learner_test3.sh \
		learner_test4.sh \
		acceptor_test1.sh \
		acceptor_test2.sh \
		acceptor_test3.sh "

export SUCCESS=0
export FAILURE=1

COUNT=0;
PASSED=0
for i in $TEST;
do
	echo "--------------------------------------";
	echo "Test $COUNT: $i";
	echo "--------------------------------------";
	
	./$i
	RESULT=$?
	
	if [[ $RESULT == $SUCCESS ]]; then
		echo " *** Successful!"
		let PASSED=PASSED+1
	else
		echo " *** Failed!"
	fi
	let COUNT=COUNT+1
done

echo "--------------------------------------";
echo "--------------------------------------";
echo "$PASSED/$COUNT Test passed"
