#include <unistd.h>
#include <stdlib.h>
#include "multicast.h"

/* 
 * Sends argv[1] hello_world messages to each multicast group
 */
#define TEST_NET "239.0.0.2", 7001

#define N_OF_MESSAGES 30

#define ARGS_ERROR 1
#define SEND_INIT_FAIL 2
#define SEND_FAIL 3
#define RECV_INIT_FAIL 4

static FILE* outputfd;

void pusage() {
	printf("#ERROR-hello_worlds.c: invalid arguments\n");
	printf("hello_worlds -s [logfile]\n");
	printf("    sends some hello_world messages to the multicast group\n");
	printf("hello_worlds -l [logfile]\n"); 
	printf("    listens for incoming messages on a multicast group\n");
	printf("Optionally prints the messages sent/received to logfile instead of stdout\n");
}

void send_msg_to_group() {
    int msg_type = 666;
	int rval, i = 0;
    multicast_sender * sender = multicast_sender_new(TEST_NET);
    if(sender == NULL) {
        printf("Sender init error\n");
        exit(SEND_INIT_FAIL);
    }

	char message[50];
	size_t msg_size = 0;
    for(i = 0; i < N_OF_MESSAGES; i++) {
        sprintf(message, "Hello World! (id: %d)", i);
        msg_size = (size_t) strlen(message);

        rval = multicast_send(sender, message, msg_size, msg_type);
        if (rval == 0) {
            fprintf(outputfd, "[t:%d][ds:%d][msg:\"Hello World! (id: %d)\"]\n",msg_type, (int)msg_size, i);
        } else {
            printf("sent failed\n");
            exit(SEND_FAIL);
        }
    }
}

void listen_on_group () {
	multicast_receiver * receiver = multicast_receiver_new(TEST_NET);
	if(receiver == NULL) {
        printf("Receiver init error\n");
        exit(RECV_INIT_FAIL);
	}
    print_receiver(receiver);

	multicast_msg * msg;
    int count = 0;
	while(count < N_OF_MESSAGES) {
		msg = multicast_recv(receiver);
		if (msg != NULL) {
            fprintf(outputfd, "[t:%d][ds:%d][msg:\"%s\"]\n",msg->type, msg->size, msg->data);
			free(msg);
            count++;
		} else {
		    printf("No messages, sleep\n");
            sleep(1);
		}
	}
}

int main (int argc, char const *argv[]) {
    
	if (argc < 2 || argc > 3) {
		pusage();
	}
	
	if (argc == 3) {
        printf("Will log to %s\n", argv[2]);
        outputfd = fopen(argv[2], "w");
	} else {
        outputfd = stdout;
	}
	
	
	if (strcmp(argv[1], "-s") == 0) {
		send_msg_to_group();
	} else if (strcmp(argv[1], "-l")  == 0) {
		listen_on_group();
	} else {
		pusage();
	}
	
    if (argc == 3) {
        fclose(outputfd);
    }

	return 0;
}

