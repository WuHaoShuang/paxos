#include <stdlib.h>
#include <stdio.h>
#include <memory.h>

#include "libpaxos_priv.h"

typedef struct acceptor_record_t {
    int     iid;
    int     ballot;
    int     value_ballot;
    int     value_size;
    char*   value;
} acceptor_record;

#include "../lib/acceptor_disk_helpers.c"

#define TEST_KEY 1234

int main (int argc, char const *argv[]) {
    
    //init DB
    disk_init_permastorage("./acceptor_disk_test.bdb");
    int i;
    acceptor_record * to_put, * got;
    
    //Make sure record does not exist
    got = disk_lookup_record(TEST_KEY);
    if (got != NULL) {
        printf("Error: get returned a value!\n");
        exit(1);
    }
    
    for(i = 0; i < 10; ++i) {
    //Create a record to write
        to_put = malloc(sizeof(acceptor_record));
        to_put->iid           = TEST_KEY;
        to_put->ballot        = 101;
        to_put->value_ballot  = 101;
        to_put->value_size    = 160;
        to_put->value = "1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890";

    //Put
        if (disk_update_record(to_put) != 0) {
            printf("Error: put failed\n");
            exit(1);
        }
        printf("Put\n");

    //Get 
        got = disk_lookup_record(to_put->iid);
        if (got == NULL) {
            printf("Error: get failed\n");
            exit(1);
        }
        printf("Get\n");
        
    //And compare with original (excluding pointer to value)
        if(memcmp(to_put, got, (sizeof(acceptor_record) - sizeof(int*)))) {
            printf("Error: Acceptor record is different!\n"); 
            printf("iid: wrote:%d got:%d\n", to_put->iid, got->iid);
            printf("ballot: wrote:%d got:%d\n", to_put->ballot, got->ballot);
            printf("val_ballot: wrote:%d got:%d\n", to_put->value_ballot, got->value_ballot);
            printf("val_size: wrote:%d got:%d\n", to_put->value_size, got->value_size);
            exit(1);
        }
    //Compare value
        if(memcmp(to_put->value, got->value, got->value_size)) {
            printf("Error: Acceptor record value is different!\n");
            exit(1);
        }

        printf("Record matches!\n");    

    //Free
        free(got->value);
        free(got);
        got = NULL;


///// PART 2    
    //Alterate original record
        to_put->ballot        = 104;
        to_put->value_ballot  = 104;

    //Put again (overwrite)
        if (disk_update_record(to_put) != 0) {
            printf("Error: put failed\n");
            exit(1);
        }
        printf("Overwrite\n");

    //Get and check
        int bal;
        bal = disk_lookup_ballot(to_put->iid);
        if (bal != to_put->ballot) {
            printf("Error: get_ballot failed\n");
            exit(1);
        }
        printf("Updated ballot matches!\n");    

    }
    
    //Close db and exit
    disk_cleanup_permastorage();
    return 0;
}

