#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "multicast.h"
#include "libpaxos_priv.h"

static char send_buffer[MC_MSG_MAX_DATA_SIZE];

void pusage() {
    printf("-n N : sends a prepare for N instances \n");
    printf("-f N : sends prepares starting from instance N and incrementing\n");
    printf("-b N : sends learns with ballot N\n");

    printf("-h   : prints this message and exits\n");
}


int n_of_prep = 1;
int first_instance_id = 0;
int ballot = 101;


void parse_args(int argc, char * const argv[]) {
    int c;
    while((c = getopt(argc, argv, "n:f:b:h")) != -1) {
        switch(c) {
            case 'n': {
                n_of_prep = atoi(optarg);
            }
            break;
            
            case 'f': {
                first_instance_id = atoi(optarg);
            }
            break;
            
            case 'b': {
                ballot = atoi(optarg);
            }
            break;
            
            case 'h':
            default: {
                pusage();
                exit(0);
            }
        }
    }    
}

// typedef struct prepare_msg_t {
//     int iid;
//     int ballot;
// } prepare_msg;
// 
// typedef struct prepare_batch_msg_t {
//     int     count;
//     char    data[0];
// } prepare_batch_msg;


void gen_and_send() {
    multicast_sender* s = multicast_sender_new(PAXOS_ACCEPTORS_NET);
    prepare_msg * prep;
    
    if (s == NULL) {
        printf("Failed to initialize sender. Exit.\n");
        exit(0);
    }
    
    int i, last_id, sres;
    prepare_batch_msg * pb = (prepare_batch_msg *)send_buffer;
    
    pb->count = 0;
    last_id = first_instance_id + n_of_prep;
    for(i = first_instance_id; i < last_id; i++) {
        prep = (prepare_msg*) &pb->data[sizeof(prepare_msg) * pb->count];
        prep->iid = i;
        prep->ballot = ballot;      
        pb->count++;
    }
    
    sres = multicast_send(s, send_buffer, (sizeof(prepare_batch_msg) + (sizeof(prepare_msg) * pb->count)), PAXOS_PREPARE);
    if(sres == -1) {
        printf("Multicast send error. Exit\n");
        exit(0);
    }

}

int main (int argc, char const *argv[]) {
    parse_args(argc, (char **)argv);
    
    printf("Will send %d prepares from iid:%d to iid:%d. Ballot:%d\n", n_of_prep, first_instance_id, (n_of_prep + first_instance_id), ballot);
    
    gen_and_send();
    return 0;
}

