#!/bin/bash

# Check dir
DIR=`pwd`;
if [ `basename $DIR` != "tests" ]; then
    echo "Please run this script from tests/"
	exit
fi

ANSWERS="$DIR/ac2_correct.log"

# Parametrize output files
OUT_ACC="$DIR/ac2_log.txt"
OUT_NET="$DIR/ac2_net.txt"
OUT_DIF="$DIR/ac2_diff.txt"


# Clear previous test results
rm -rf $OUT_ACC $OUT_NET $OUT_DIF

# Start and detach process, save pid
echo "Starting network logger"
./packet_logger -f $OUT_NET > /dev/null &
NET_PID=$!;

# Start and detach process, save pid
echo "Starting network acceptor"
./acceptor_main > $OUT_ACC &
ACC_PID=$!;
sleep 2

echo "Send valid prepare + accept"
./gen_prep -f 1 -n 1 -b 101 > /dev/null
sleep 1
./gen_accept -i 1 -b 101 -v "test_value_1" > /dev/null
sleep 1

echo "Send send accept without promise"
./gen_accept -i 2 -b 101 -v "test_value_2" > /dev/null
sleep 1

echo "Then send prepare with higher ballot"
./gen_prep -f 2 -n 1 -b 201 > /dev/null
sleep 1

echo "And corresponding accept with same ballot"
./gen_accept -i 2 -b 201 -v "test_value_3" > /dev/null
sleep 1


# Wait for test termination
sleep 3
# Kill Processes
kill -s INT $ACC_PID $NET_PID

# Check results

diff $OUT_NET $ANSWERS > $OUT_DIF
D_LINES=`cat $OUT_DIF | wc -l`; let D_LINES=D_LINES+0
if [[ $D_LINES == 0 ]]; then
	echo "No differences"
else
	echo "There are differences in output!"
	echo "Check diff file $OUT_DIF"
	exit $FAILURE	
fi

# Return $SUCCESS or $FAILURE
exit $SUCCESS
