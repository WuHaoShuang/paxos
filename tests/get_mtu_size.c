#include <stdio.h>
#include <sys/socket.h>

/*
    Reads value of maximum packet size accepted by sendto()
*/

int main (int argc, char const *argv[])
{
    int sock;
    socklen_t optlen = sizeof(int);
    int optval;
    
    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
        perror("socket creation");
        return -1;
    }
    
/* Linux */ 
    // getsockopt(sock, SOL_SOCKET, SO_MAX_MSG_SIZE, (int *)&optval, &optlen);
    // printf("SOL_SOCKET:SO_MAX_MSG_SIZE:%d\n", optval);

/* Mac */
    getsockopt(sock, SOL_SOCKET, SO_SNDBUF, (int *)&optval, &optlen);
    printf("SOL_SOCKET:SO_SNDBUF:%d\n", optval);
    return 0;
}