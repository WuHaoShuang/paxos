#include <unistd.h>
#include <stdio.h>
#include <signal.h>

#include "libpaxos_priv.h"
#include "multicast.h"

#define RECV_INIT_FAIL 1

#define TEST_NET "239.0.0.2", 7001

static multicast_receiver* recvs[4];
char* nets_names[4] = {"LEAS", "ACCS", "PROP", "TEST"};

static FILE* outfd = NULL;

static int print_no_val = 0;
void print_value_string(FILE* fd, char* str, int str_size) {
    if (print_no_val) {
        fprintf(fd, "\"...\"");
        return;
    }
        
    
    if(str_size == 0) {
        fprintf(fd, "\"NO_VALUE\"");
        return;
    }
    
    putc('"', fd);
    int i;
    for(i = 0; i < str_size; ++i) {
        if(str[i] > 32 && str[i] < 127)
            putc(str[i], fd);
        else
            fputc('!', fd);
            // fprintf(fd, "\\%d", (int)str[i]);

    }
    putc('"', fd);
}

void handle_cltr_c (int sig) {
	LOG (VRB, ("Caught exit signal\n"));
	if(outfd != stdout)
        fclose(outfd);    
    exit (0);
}

void print_paxos_msg(multicast_msg * msg) {
    int i;
    int offset = 0;
    
    switch(msg->type) {
        case PAXOS_PREPARE: {
            fprintf(outfd, "{prepare_batch [s:%d], ", msg->size);
            prepare_batch_msg * pb_msg = (prepare_batch_msg *)msg->data;
            for(i = 0; i < pb_msg->count; i++) {
                prepare_msg * pm = (prepare_msg *)&pb_msg->data[offset];
                fprintf(outfd, "(%d){ iid:%d bal:%d } ", i+1, pm->iid, pm->ballot);
                offset += sizeof(prepare_msg);
            }
            fprintf(outfd, "}\n");
        } 
        break;
        
        case PAXOS_ACCEPT:  {
            fprintf(outfd, "{accept [s:%d], ", msg->size);
            accept_msg * ac_msg = (accept_msg *)msg->data;
            fprintf(outfd, "iid:%d bal:%d val:", ac_msg->iid, ac_msg->ballot);
            print_value_string(outfd, ac_msg->value, ac_msg->value_size);
            fprintf(outfd, " [s:%d] }\n", ac_msg->value_size);
        } break;
        
        case PAXOS_LSYNC:   {
            fprintf(outfd, "{learner_sync [s:%d], ", msg->size);
            learner_sync_msg * ls_msg = (learner_sync_msg *)msg->data;
            int cid;
            for(i = 0; i < ls_msg->count; i++) {
                cid = ls_msg->ids[i];
                fprintf(outfd, "(%d){ iid:%d } ", i+1, cid);
            }
            fprintf(outfd, "}\n");
        } 
        break;
        
        case PAXOS_PROMISE: {
            fprintf(outfd, "{promise_batch [s:%d], ", msg->size);
            promise_batch_msg * pb_msg = (promise_batch_msg *)msg->data;
            fprintf(outfd, "acceptor:%d, ", pb_msg->acceptor_id);
            for(i = 0; i < pb_msg->count; i++) {
                promise_msg * pm = (promise_msg *)&pb_msg->data[offset];
                fprintf(outfd, "(%d){ iid:%d bal:%d val:", i+1, pm->iid, pm->ballot);
                print_value_string(outfd, pm->value, pm->value_size);
                fprintf(outfd, "[s:%d, bal:%d] } ", pm->value_size, pm->value_ballot);
                offset += (sizeof(promise_msg) + pm->value_size);
            }
            fprintf(outfd, "}\n");
        } 
        break;
        
        case PAXOS_LEARN:   {
            fprintf(outfd, "{learn [s:%d], ", msg->size);
            learn_msg * l_msg = (learn_msg *)msg->data;
            fprintf(outfd, "iid:%d bal:%d val:", l_msg->iid, l_msg->ballot);
            print_value_string(outfd, l_msg->value, l_msg->value_size);
            fprintf(outfd, " [s:%d] }\n", l_msg->value_size);   
        }
        break;
        
        default: {
            fprintf(outfd, "Unkow message type:%d [s:%d]\n", msg->type, msg->size);
        }
    }
}
                
void pusage() {
    printf("-f filename : Logs to filename instead of stdout\n");
    printf("-n          : Dont print values\n");
    printf("-h          : prints this message\n");
}

void parse_args(int argc, char * const argv[]) {
    int c;
    while((c = getopt(argc, argv, "f:nh")) != -1) {
        switch(c) {
            case 'f': {
                outfd = fopen(optarg, "w");
                if (outfd == NULL) {
                    perror("opening output file");
                    exit(1);
                }
                
            }
            break;

            case 'n': {
                print_no_val = 1;
            }
            break;

            
            case 'h':
            default: {
                pusage();
                exit(0);
            }
        }
    }
}

int main (int argc, char const *argv[]) {
    signal (SIGINT, handle_cltr_c);
    
    parse_args(argc, (char **)argv);
    if (outfd == NULL) outfd = stdout;
    

        
    recvs[0] = multicast_receiver_new(PAXOS_LEARNERS_NET);
    if(recvs[0] == NULL) {
        printf("Receiver init error\n");
        exit(RECV_INIT_FAIL);
    }

    recvs[1] = multicast_receiver_new(PAXOS_ACCEPTORS_NET);
    if(recvs[1] == NULL) {
        printf("Receiver init error\n");
        exit(RECV_INIT_FAIL);
    }

    recvs[2] = multicast_receiver_new(PAXOS_PROPOSERS_NET);
    if(recvs[2] == NULL) {
        printf("Receiver init error\n");
        exit(RECV_INIT_FAIL);
    }

    //Group used by other tests
    recvs[3] = multicast_receiver_new(TEST_NET);
    if(recvs[3] == NULL) {
        printf("Receiver init error\n");
        exit(RECV_INIT_FAIL);
    }


    multicast_msg * msg;
    int got_something = 0;
    while(1) {
        
        int i;
        //Check for msg on 3 nets and print them
        for(i = 0; i < 4; i++) {
            msg = multicast_recv (recvs[i]);
            if(msg != NULL) {
                got_something = 1;
                fprintf(outfd, "N:%s -> ", nets_names[i]);
                print_paxos_msg(msg);
            }
        }

        //Sleep if none of the nets had message
        if(got_something == 0) {
            usleep(50000);
        } else {
            got_something = 0;
        }

    }
}

