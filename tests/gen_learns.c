#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#include "multicast.h"
#include "libpaxos_priv.h"

void pusage() {
    printf("-a N : sends N learns from N acceptors\n");
    printf("-f N : forces all messages to be sent with acceptor id N\n");
    printf("-i N : sends learns for instance id N\n");
    printf("-b N : sends learns with ballot N\n");
    printf("-r N : generates a random value of size N\n");
    printf("-v val : sends val as value in the learns\n");
    printf("-h   : prints this message and exits\n");
}


int n_of_learns = N_OF_ACCEPTORS;
int instance_id = 0;
int ballot = 101;
int value_size = 10;
char* value = "123456789";
int random_value = 1;
int fixed_acceptor_id = -1;

char* gen_random_val() {
    int i;
    char* str;
    
    str =  malloc(value_size);
    srand(time(NULL));
    
    for(i = 0; i < value_size; i++) {
        str[i] = (char) ((rand() % 25) + 65);
    }
    return str;
}

void parse_args(int argc, char * const argv[]) {
    int c;
    while((c = getopt(argc, argv, "a:f:i:b:r:v:h")) != -1) {
        switch(c) {
            case 'a': {
                n_of_learns = atoi(optarg);
            }
            break;
            case 'f': {
                fixed_acceptor_id = atoi(optarg);
            }
            break;
            case 'i': {
                instance_id = atoi(optarg);
            }
            break;

            case 'b': {
                ballot = atoi(optarg);
            }
            break;

            case 'r': {
                value_size = atoi(optarg);
            }
            break;

            case 'v': {
                value_size = strlen(optarg) + 1;
                value = optarg;
                random_value = 0;
            }
            break;
            
            case 'h':
            default: {
                pusage();
                exit(0);
            }
        }
        if(random_value) {
            gen_random_val();
        }
    }    
}

char send_buffer[MC_MSG_MAX_DATA_SIZE];

// typedef struct learn_msg_t {
//     int     acceptor_id;
//     int     iid;
//     int     ballot;
//     int     value_size;
//     char    value[0];
// } learn_msg;

void gen_and_send() {
    multicast_sender* s = multicast_sender_new(PAXOS_LEARNERS_NET);
    if (s == NULL) {
        printf("Failed to initialize sender. Exit.\n");
        exit(0);
    }


    int i, sres;
    learn_msg * lmsg = (learn_msg*)send_buffer;
    lmsg->iid = instance_id;
    lmsg->ballot = ballot;
    lmsg->value_size = value_size;
    memcpy(lmsg->value, value, value_size);    
    for(i = 0; i < n_of_learns; i++) {
        
        if(fixed_acceptor_id == -1) 
            lmsg->acceptor_id = i;
        else
            lmsg->acceptor_id = fixed_acceptor_id;
            
        sres = multicast_send(s, send_buffer, (sizeof(learn_msg) + value_size), PAXOS_LEARN);
        if(sres == -1) {
            printf("Multicast send error. Exit\n");
            exit(0);
        }
    }
}

int main (int argc, char const *argv[]) {
    parse_args(argc, (char **)argv);
    
    printf("Will send %d learn for instance %d with ballot %d. Value: \"%s\"[%d]\n", n_of_learns, instance_id, ballot, value, value_size);
    
    gen_and_send();
    return 0;
}

