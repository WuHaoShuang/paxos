#include "libpaxos_priv.h"
#include "multicast.h"

multicast_receiver * mcast_receiver_new (char * group, int port) {
      struct ip_mreq mreq;
      multicast_receiver * receiver = malloc(sizeof(multicast_receiver));

      if (receiver == NULL) {
          printf("Error allocating receiver\n");
          return NULL;
      }

      memset(&mreq, '\0', sizeof(struct ip_mreq));
      memset(receiver, '\0', sizeof(multicast_receiver));

      receiver->sock = socket(AF_INET, SOCK_DGRAM, 0);
      if (receiver->sock < 0) {
          perror("receiver socket");
          free(receiver);
          return NULL;
      }
	
      int activate = 1;
      if (setsockopt(receiver->sock, SOL_SOCKET, SO_REUSEADDR, &activate, sizeof(int)) != 0) {
          perror("setsockopt, setting SO_REUSEADDR");
          free(receiver);
          return NULL;
      }

      mreq.imr_multiaddr.s_addr = inet_addr(group);         
      mreq.imr_interface.s_addr = htonl(INADDR_ANY);
      activate = setsockopt(receiver->sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
      if (activate != 0) {
          perror("setsockopt, setting IP_ADD_MEMBERSHIP");
          free(receiver);
          return NULL;
      }

      receiver->addr.sin_addr.s_addr = inet_addr(group);
      if (receiver->addr.sin_addr.s_addr == INADDR_NONE) {
          printf("Error setting receiver->addr\n");
          free(receiver);
          return NULL;
      }
      receiver->addr.sin_family = AF_INET;
      receiver->addr.sin_port = htons((uint16_t)port);	
      receiver->addrlen = sizeof(receiver->addr);


      snprintf(receiver->addr_string, MC_ADDR_STRING_SIZE, "%s", group);
      receiver->addr_string[MC_ADDR_STRING_SIZE - 1] = '\0';
      receiver->port = port;	

      receiver->recv_buffer = malloc(sizeof(multicast_msg) + MC_MSG_MAX_DATA_SIZE);
      if (receiver->recv_buffer == NULL) {
          printf("Error allocating receive buffer\n");
          free(receiver);
          return NULL;
      }

      receiver->list_head = NULL;
      receiver->list_tail = NULL;


      if (bind(receiver->sock, (struct sockaddr *) &receiver->addr, sizeof(receiver->addr)) != 0) {
          perror("bind");
          free(receiver->recv_buffer);
          free(receiver);
          return NULL;
      }

      if(VERBOSITY_LEVEL > 3) print_receiver(receiver);
      
      return receiver;
}

multicast_msg * mcast_recv (multicast_receiver * r) {
    int msg_size = 0;
    int buf_size = sizeof(multicast_msg) + MC_MSG_MAX_DATA_SIZE;
    
    msg_size = recvfrom(r->sock, r->recv_buffer, buf_size, MSG_WAITALL, (struct sockaddr *)&r->addr, &r->addrlen);
    
    if (msg_size < 0) {
        perror("recvfrom");
        return NULL;
    }
    
    if (msg_size == 0) {
        printf("mcast: got message of size 0!\n");
        return NULL;
    }
    
    return r->recv_buffer;
}
