#include "libpaxos_priv.h"

#include "multicast.h"

multicast_sender * multicast_sender_new (char * group, int port) {

    multicast_sender * sender = malloc(sizeof(multicast_sender));
    if (sender == NULL) {
        printf("Error allocating sender\n");
        return NULL;
    }

    memset(sender, '\0', sizeof(multicast_sender));

    sender->sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sender->sock < 0) {
        perror("sender socket");
        free(sender);
        return NULL;
    }
    sender->addr.sin_addr.s_addr = inet_addr(group);
    if (sender->addr.sin_addr.s_addr == INADDR_NONE) {
        printf("Error setting sender->addr\n");
        free(sender);
        return NULL;
    }
    sender->addr.sin_family = AF_INET;
    sender->addr.sin_port = htons((uint16_t)port);	
    sender->addrlen = sizeof(struct sockaddr_in);



    snprintf(sender->addr_string, MC_ADDR_STRING_SIZE, "%s", group);
    sender->addr_string[MC_ADDR_STRING_SIZE - 1] = '\0';
    sender->port = port;

    sender->send_buffer = malloc(sizeof(multicast_msg) + MC_MSG_MAX_DATA_SIZE);
    if (sender->send_buffer == NULL) {
        printf("Error allocating sender buffer\n");
        free(sender);
        return NULL;
    }

    if(VERBOSITY_LEVEL > 3) print_sender(sender);
    return sender;
}

int multicast_send (multicast_sender * sender, char * message, size_t msg_size, int msg_type) {
    int packet_size; 
    
    if (msg_size > MC_MSG_MAX_DATA_SIZE) {
     printf("Error: message too big! (max msg_size = %u)\n", (uint)(MC_MSG_MAX_DATA_SIZE));
     return -1;
    }
    
        multicast_msg * send_msg = sender->send_buffer;
    memcpy(send_msg->data, message, msg_size);
        send_msg->size = msg_size;
        send_msg->type = msg_type;
        packet_size = (int)MULTICAST_MSG_SIZE(send_msg);
    
    int cnt = -1;    
    cnt = sendto(sender->sock, send_msg, packet_size, 0, (struct sockaddr *) &sender->addr, sender->addrlen);
    if (cnt != packet_size || cnt == -1) {
        perror("paxnet_sendmsg");
        return -1;
    }
	
    LOG(DBG, ("multicast_send -> sent packet of size %d\n", packet_size));
	return 0;
}

void print_sender (multicast_sender * sender) {
	printf("@%p\t+Sender \n", (void*)sender);
	printf("@%p\t|--sock = %d\n", (void*)&sender->sock, (int)sender->sock);
	printf("@%p\t|--addrlen = %d\n", (void*)&sender->addrlen, (int)sender->addrlen);
	printf("@%p\t|-+addr TODO\n", (void*)&sender->addr);
	printf("@%p\t  |--sin_family = %d ", (void*)&(sender->addr).sin_family, (int)(sender->addr).sin_family);
	  printf("[AF_INET = %d]\n", (int)AF_INET);
	printf("@%p\t  |--sin_port = %d) ", (void*)&(sender->addr).sin_port, (int)(sender->addr).sin_port);
	  printf("[htons(port) = %d]\n", (int)htons(sender->port));
	printf("@%p\t  |-+sin_addr\n", (void*)&(sender->addr).sin_addr);
	printf("@%p\t    |--s_addr = %d ", (void*)&(sender->addr).sin_addr.s_addr, (int)(sender->addr).sin_addr.s_addr);
    printf("[inet_addr(group) = %d]\n", (int)inet_addr(sender->addr_string));	
	printf("@%p\t|-+addr_str = \"%s\"\n", (void*)&sender->addr_string, sender->addr_string);
	printf("@%p\t|-+port = %d)\n", (void*)&sender->port, sender->port);
}

