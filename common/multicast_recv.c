#include <pthread.h>

#include "libpaxos_priv.h"
#include "multicast.h"

void enqueue_message(multicast_receiver* receiver, int size) {

    multicast_msg* msg = receiver->recv_buffer;
    multicast_msg* new_msg = malloc(size);
    memcpy(new_msg, msg, size);



    mc_msg_wrapper* w = malloc(sizeof(mc_msg_wrapper));
    w->next = NULL;
    w->msg = new_msg;

    pthread_mutex_lock( &receiver->list_mutex );

    if(receiver->list_head == NULL && receiver->list_tail == NULL) {
        receiver->list_head = w;
        receiver->list_tail = w;
    } else if (receiver->list_head != NULL && receiver->list_tail != NULL) {
        receiver->list_tail->next = w;
        receiver->list_tail = w;
    } else {
        printf("Error: packet queue is in inconsistent state!\n");
        free(new_msg);
        free(w);
    }
    
    pthread_mutex_unlock( &receiver->list_mutex );
    if (DEBUG_RECV_THREAD) {
        LOG(DBG, ("enqueue_message -> added packet of size %d to queue\n", size));
    }
}

multicast_msg * dequeue_message(multicast_receiver* receiver) {
    mc_msg_wrapper * w;
    multicast_msg * msg;

    pthread_mutex_lock( &receiver->list_mutex );

    if(receiver->list_head == NULL && receiver->list_tail == NULL) {
        pthread_mutex_unlock( &receiver->list_mutex );
        return NULL;
    } 
    w = receiver->list_head;
    if (w == receiver->list_tail && w->next == NULL) { 
        receiver->list_head = NULL;
        receiver->list_tail = NULL;
    } else {
        receiver->list_head = w->next;
    }

    pthread_mutex_unlock(&receiver->list_mutex);
    msg = w->msg;
    free(w);

    if (DEBUG_RECV_THREAD) {
    LOG(DBG, ("dequeue_message -> returning message of size %d\n", (int)MULTICAST_MSG_SIZE(msg)));
    }
    
    return msg;
}


void * mc_receiver_loop(void * receiver_arg) {
    multicast_receiver * receiver = receiver_arg;

    size_t psize = 0;
    while(1) {
        psize = recvfrom(receiver->sock, receiver->recv_buffer, (sizeof(multicast_msg) + MC_MSG_MAX_DATA_SIZE), MSG_WAITALL, (struct sockaddr *) &receiver->addr, &receiver->addrlen);
        if (psize < 0) {
            perror("recvfrom");
        } else if (psize > 0) {
            enqueue_message(receiver, psize);
        } else {
            printf("Receiver: got message of size 0!\n");
        }	
    }

    return NULL;
}

multicast_receiver * multicast_receiver_new (char * group, int port) {
    struct ip_mreq mreq;
    multicast_receiver * receiver = malloc(sizeof(multicast_receiver));

    if (receiver == NULL) {
        printf("Error allocating receiver\n");
        return NULL;
    }

    memset(&mreq, '\0', sizeof(struct ip_mreq));
    memset(receiver, '\0', sizeof(multicast_receiver));

    receiver->sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (receiver->sock < 0) {
        perror("receiver socket");
        free(receiver);
        return NULL;
    }


    int activate = 1;
    if (setsockopt(receiver->sock, SOL_SOCKET, SO_REUSEADDR, &activate, sizeof(int)) != 0) {
        perror("setsockopt, setting SO_REUSEADDR");
        free(receiver);
        return NULL;
    }


    mreq.imr_multiaddr.s_addr = inet_addr(group);         
    mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    activate = setsockopt(receiver->sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(mreq));
    if (activate != 0) {
        perror("setsockopt, setting IP_ADD_MEMBERSHIP");
        free(receiver);
        return NULL;
    }


    receiver->addr.sin_addr.s_addr = inet_addr(group);
    if (receiver->addr.sin_addr.s_addr == INADDR_NONE) {
        printf("Error setting receiver->addr\n");
        free(receiver);
        return NULL;
    }
    receiver->addr.sin_family = AF_INET;
    receiver->addr.sin_port = htons((uint16_t)port);	
    receiver->addrlen = sizeof(receiver->addr);



    snprintf(receiver->addr_string, MC_ADDR_STRING_SIZE, "%s", group);
    receiver->addr_string[MC_ADDR_STRING_SIZE - 1] = '\0';
    receiver->port = port;	

    receiver->recv_buffer = malloc(sizeof(multicast_msg) + MC_MSG_MAX_DATA_SIZE);
    if (receiver->recv_buffer == NULL) {
        printf("Error allocating receive buffer\n");
        free(receiver);
        return NULL;
    }

    receiver->list_head = NULL;
    receiver->list_tail = NULL;

    if (bind(receiver->sock, (struct sockaddr *) &receiver->addr, sizeof(receiver->addr)) != 0) {
        perror("bind");
        free(receiver->recv_buffer);
        free(receiver);
        return NULL;
    }

    if (pthread_mutex_init(&receiver->list_mutex, NULL) != 0) {
        perror("cannot init lock paxnet_initialize_receiver");
        free(receiver->recv_buffer);
        free(receiver);
        return NULL;
    }

    if (pthread_create(&receiver->recv_thread, NULL, mc_receiver_loop, (void*) receiver) != 0) {
        perror("paxnet_initialize_receiver");
        free(receiver->recv_buffer);
        free(receiver);
        return NULL;
    }
    
    if(VERBOSITY_LEVEL > 3) print_receiver(receiver);
    return receiver;
}

multicast_msg * multicast_recv (multicast_receiver * receiver) {
    return dequeue_message(receiver);
}

void print_receiver (multicast_receiver * receiver) {
	printf("@%p\t+Receiver (size: %d)\n", (void*)receiver, (int)sizeof(multicast_receiver));
	printf("@%p\t|--sock = %d\n", (void*)&receiver->sock, (int)receiver->sock);
	printf("@%p\t|--addrlen = %d\n", (void*)&receiver->addrlen, (int)receiver->addrlen);
	printf("@%p\t|-+addr TODO\n", (void*)&receiver->addr);
	printf("@%p\t  |--? \n", (void*)&receiver->addr);
	printf("@%p\t  |--sin_family = %d ", (void*)&(receiver->addr).sin_family, (int)(receiver->addr).sin_family);
	  printf("(AF_INET = %d)\n", (int)AF_INET);
    printf("@%p\t  |--sin_port = %d ", (void*)&(receiver->addr).sin_port, (int)(receiver->addr).sin_port);
      printf("[htons(port) = %d]\n", (int)htons(receiver->port));
      
    printf("@%p\t  |-+sin_addr\n", (void*)&(receiver->addr).sin_addr);

    printf("@%p\t    |--s_addr = %d ", (void*)&(receiver->addr).sin_addr.s_addr, (int)(receiver->addr).sin_addr.s_addr);
      printf("[inet_addr(group) = %d]\n", (int)inet_addr((char*)&receiver->addr_string));    
      
    printf("@%p\t|-+addr_str val=\"%s\"\n", (void*)&receiver->addr_string, receiver->addr_string);
    printf("@%p\t|-+port = %d\n", (void*)&receiver->port, receiver->port);
}

