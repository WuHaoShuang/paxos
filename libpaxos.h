#ifndef _LIBPAXOS_H_
#define _LIBPAXOS_H_

typedef void (* deliver_function)(char*, int);

/*
    Max size in bytes of values to propose.
    This must be smaller or equal to multicast.h parameter MC_MSG_MAX_DATA_SIZE.
    i.e. for MTU = MC_MSG_MAX_DATA_SIZE = 4000
    PAXOS_MAX_VALUE_SIZE = 4000 - 8 (header) - 32 (biggest paxos header) = 3960.
    This value is here since the client may need it.
*/
#define PAXOS_MAX_VALUE_SIZE 8960

/* ***  PROPOSER  *** */

/*
    Start a proposer in the current process and returns
*/
int proposer_init(int proposer_id);

/*
    As before but specifies a deliver function to be invoked when the
    internal learner delivers a value.
    IMPORTANT: F is invoked by an internal proposer thread, pay 
    attentions to race conditions with your own threads.
    While F is invoked, part of the proposer is blocked,
    do what you need to do as fast as possible!
    (The buffer passed as argument should not bee freed)
    (calling with NULL f is equivalent to proposer_init())
*/
int proposer_init_and_deliver(int proposer_id, deliver_function f);


/*
    Send to the local proposer a value to be delivered trough Paxos.
    This call is thread-safe and returns immediately
*/
int proposer_submit_value(char* value, int size);



/* ***  ACCEPTOR  *** */

/*
    Initializes an acceptor in the current process.
*/
int acceptor_init(int id);

/*
    Starts acceptor. returns only if an error occurs.
*/
int acceptor_start();



/* ***  LEARNER  *** */

/*
    Starts a learner in the current process.
*/
int learner_init();

/*
    Get the next value delivered trough Paxos. Values are delivered in order.
    Will not return until the value for the next instance is decided.
*/
int learner_get_next_value(char** valuep);

/*
    This is a non-blocking version that will set valuep to NULL if the next 
    value is not delivered yet.
*/
int learner_get_next_value_nonblock(char** valuep);



/* ***  DEBUG  *** */

/*
    Get the current queue size of client values in the proposer 
*/
int proposer_queue_size();

/*
    Print the current values for the event counters of the proposer, 
    if event counting is enabled
*/
void proposer_print_event_counters();

/*
    Print the current values for the event counters of the learner, 
    if event counting is enabled
*/
void learner_print_event_counters();

/* 
	Alternative API to submit multiple values locking a single time.
	Only for debugging/benchmarking purposes.
	To submit multiple values in an efficent way, merge them into a single value 
*/
int lock_proposer_submit();
int nolock_proposer_submit_value(char * value, int size);
int unlock_proposer_submit();

#endif /* _LIBPAXOS_H_ */
